$('#imgfiles').on('change', function (e) {
    renderImages();
})
$('#img-view').on('click', '.removeImage', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfiles')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfiles')[0].files[i]);
        }
    }
    $('#imgfiles')[0].files = dt.files;
    renderImages();
})
function renderImages(){
    let el = $('#img-view');
    el.html(``);
    for (let i=0; i<$('#imgfiles')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfiles')[0].files[i])
        el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImage" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
    }
}

// ==============================================================================================================

$('#imgfilesOne').on('change', function (e) {
    renderImagesOne();
})
$('#img-view-one').on('click', '.removeImageOne', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfilesOne')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfilesOne')[0].files[i]);
        }
    }
    $('#imgfilesOne')[0].files = dt.files;
    renderImagesOne();
})
function renderImagesOne(){
    let el = $('#img-view-one');
    el.html(``);
    for (let i=0; i<$('#imgfilesOne')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfilesOne')[0].files[i])
        el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOne" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
    }
}


// ==============================================================================================================



$('#imgfilesOneHeader').on('change', function (e) {
    renderImagesOneHeader();
})
$('#img-view-one-header').on('click', '.removeImageOneHeader', function () {
    let remIndex = Number($(this).attr('data-value'));
    let dt = new DataTransfer();
    for (let i=0; i<$('#imgfilesOneHeader')[0].files.length; i++){
        if (i !== remIndex){
            dt.items.add($('#imgfilesOneHeader')[0].files[i]);
        }
    }
    console.log($('#imgfilesOneHeader')[0].files);
    $('#imgfilesOneHeader')[0].files = dt.files;

    renderImagesOneHeader();
})
function renderImagesOneHeader(){
    let el = $('#img-view-one-header');
    el.html(``);
    for (let i=0; i<$('#imgfilesOneHeader')[0].files.length; i++){
        let src = URL.createObjectURL($('#imgfilesOneHeader')[0].files[i])
        let type = $('#imgfilesOneHeader')[0].files[0].type;
        console.log(type.split('/'))
        if (type.split('/')[0] == 'video'){
            el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOneHeader" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red; z-index: 999999">x</div>
                         <video width="400" controls style="width: 160px;">
                             <source src="${src}" type="${type}">
                         </video>
                    </div>
                `);
        }else{
            el.append(`
                    <div style="display:flex; float: left; margin: 10px; position:relative">
                         <div class="removeImageOneHeader" data-value="${i}" style="position:absolute; top: 5px; background: #0c0c0c85; left: 5px; border: 1px solid red; cursor: pointer;width: 29px; height: 29px; text-align: center; color:red">x</div>
                         <img style="width: 160px;" src="${src}">
                    </div>
                `);
        }

    }
}

// ==============================================================================================================
