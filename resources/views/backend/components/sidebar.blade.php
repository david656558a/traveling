<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/admin" class="brand-link">
        <img src="{{asset('assets/backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">Admin panel</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('assets/backend/custom/image/Businessman-160x160.png')}}" class="img-circle elevation-2" alt="User Image" style="background-color: white;">
            </div>
            <div class="info">
                <a href="#" class="d-block">Administrator</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{route('domain.index')}}" class="nav-link {{ Route::currentRouteNamed(['domain.index', 'domain.edit', 'domain.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-bezier-curve"></i>
                        <p>
                            Domain
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('header.index')}}" class="nav-link {{ Route::currentRouteNamed(['header.index', 'header.edit', 'header.create']) ? 'active' : '' }}">
                        <i class="nav-icon far fa-image"></i>
                        <p>
                            Header
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('destination.index')}}" class="nav-link {{ Route::currentRouteNamed(['destination.index', 'destination.edit', 'destination.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-arrows-turn-to-dots"></i>
                        <p>
                            Destinations
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('news.index')}}" class="nav-link {{ Route::currentRouteNamed(['news.index', 'news.edit', 'news.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-diagram-next"></i>
                        <p>
                            News
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('info.index')}}" class="nav-link {{ Route::currentRouteNamed(['info.index', 'info.edit', 'info.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-hashtag"></i>
                        <p>
                            Info
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('content.index')}}" class="nav-link {{ Route::currentRouteNamed(['content.index', 'content.edit', 'content.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-align-left"></i>
                        <p>
                            Content
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('pages.index')}}" class="nav-link {{ Route::currentRouteNamed(['pages.index', 'pages.edit', 'pages.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-file"></i>
                        <p>
                            Pages
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('brochure.index')}}" class="nav-link {{ Route::currentRouteNamed(['brochure.index', 'brochure.edit', 'brochure.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-pager"></i>
                        <p>
                            Brochure
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('footer.index')}}" class="nav-link {{ Route::currentRouteNamed(['footer.index', 'footer.edit', 'footer.create']) ? 'active' : '' }}">
                        <i class="nav-icon fa-solid fa-arrows-down-to-line"></i>
                        <p>
                            Footer
                        </p>
                    </a>
                </li>






                {{--<li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-envelope"></i>
                        <p>
                            Droup Down
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Inbox</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Compose</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Read</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-image"></i>
                        <p>
                            Single
                        </p>
                    </a>
                </li>--}}
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
