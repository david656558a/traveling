@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')

@endsection



@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('content.index')}}">Content</a></li>
                                <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Create</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('content.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    @include("backend.components.translate")
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-two-tabContent">
                                            @foreach($domainsGLOBAL as $key => $item)
                                                <div class="tab-pane fade  @if($key == 0) show active @endif" id="{{$item->lang}}" role="tabpanel" aria-labelledby="custom-tabs-two-{{$item->lang}}-tab">
                                                    <div class="card-body row">
                                                        <div class="form-group col-3">
                                                            <label for="exampleInputPassword1">Title</label>
                                                            <input type="text" class="form-control" name="langs[{{ $item->lang }}][title]" id="exampleInputPassword1" placeholder="Title" value="{{ old('langs['.$item->lang.'][title]') }}"  >
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <label for="exampleInputPassword21">Description</label>
                                                            <textarea class="summernote" name="langs[{{ $item->lang }}][description]">{{ old('langs['.$item->lang.'][description]') }}</textarea>
                                                        </div>
                                                        <input type="hidden" style="display: none" name="langs[{{ $item->lang }}][domain_id]" value="{{$item->id}}">
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image_one" id="imgfilesOne">
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


