@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('info.index')}}">Info</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Edit</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('info.update', $info->id)}}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    <div class="card-body row">
                                        <div class="form-group col-4">
                                            <label>Youtube</label>
                                            <input type="text" name="youtube" class="form-control" placeholder="URL" value="{{$info->youtube}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Twitter</label>
                                            <input type="text" name="twitter" class="form-control" placeholder="URL" value="{{$info->twitter}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Facebook</label>
                                            <input type="text" name="facebook" class="form-control" placeholder="URL" value="{{$info->facebook}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Instagram</label>
                                            <input type="text" name="instagram" class="form-control" placeholder="URL" value="{{$info->instagram}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Phone</label>
                                            <input type="text" name="phone" class="form-control" placeholder="Phone number ( + XXXX )" value="{{$info->phone}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Email</label>
                                            <input type="text" name="email" class="form-control" placeholder="Email" value="{{$info->email}}">
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Time</label>
                                            <input type="text" name="time" class="form-control" placeholder="Monday to Friday / 09:00 AM - 06:00 PM (EST)" value="{{$info->time}}">
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-success">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


