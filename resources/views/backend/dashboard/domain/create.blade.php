@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/bankCard/dist/css/DatPayment.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/bankCard/example.css')}}">
    <style>
        .password_info{
            color: red;
            font-size: 12px;
            margin-left: 15px;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Create</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
            <form action="{{route('domain.store')}}" method="post" id="quickForm">
                @method('POST')
                @csrf
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h3 class="card-title">Domain</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Domain</label>
                                            <input type="text" name="domain" class="form-control" placeholder="Enter Domain ( www.google.com )" value="{{old('domain')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{old('name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Lang</label>
                                            <input type="text" name="lang" class="form-control" placeholder="Enter Lang ( es )" value="{{old('lang')}}">
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </div>
                </section>
            </form>
    <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $('#quickForm').validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    password: {
                        minlength: 5
                    },
                    subscription_id: {
                        required: true
                    },

                },
                messages: {
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a vaild email address"
                    },
                    cvv: {
                        maxlength: "Please enter a vaild email address",
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection


