@extends('layouts.dashboard')


@section('title')
    <title>Edit</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
@endsection

@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('footer.index')}}">Footer</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Edit</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('footer.update', $footer->id)}}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @csrf
                                    @include("backend.components.translate")
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-two-tabContent">
                                            @foreach($domainsGLOBAL as $key => $item)
                                                <div class="tab-pane fade  @if($key == 0) show active @endif" id="{{$item->lang}}" role="tabpanel" aria-labelledby="custom-tabs-two-{{$item->lang}}-tab">
                                                    <div class="card-body row">
                                                        <div class="card-body row">
                                                            <div class="form-group col-12">
                                                                <label for="exampleInputPassword21">Description</label>
                                                                <textarea class="summernote" name="langs[{{ $item->lang }}][description]">{{isset($footer->translateLang[$key]->description) ? $footer->translateLang[$key]->description : ''}}</textarea>
                                                            </div>
                                                            <div class="form-group col-6">
                                                                <label for="exampleInputPassword22">Down footer one</label>
                                                                <input type="text" class="form-control" name="langs[{{ $item->lang }}][down_footer_one]" id="exampleInputPassword22" placeholder="Down footer one" value="{{isset($footer->translateLang[$key]->down_footer_one) ? $footer->translateLang[$key]->down_footer_one : ''}}">
                                                            </div>
                                                            <div class="form-group col-6">
                                                                <label for="exampleInputPassword23">Down footer two</label>
                                                                <input type="text" class="form-control" name="langs[{{ $item->lang }}][down_footer_two]" id="exampleInputPassword23" placeholder="Down footer two" value="{{isset($footer->translateLang[$key]->down_footer_two) ? $footer->translateLang[$key]->down_footer_two : ''}}">
                                                            </div>
                                                            <input type="hidden" style="display: none" name="langs[{{ $item->lang }}][domain_id]" value="{{$item->id}}">
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Images</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image_one" id="imgfilesOne">
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                        <img style="width: 50%" src="{{asset($footer->path)}}" alt="">
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')

@endsection


