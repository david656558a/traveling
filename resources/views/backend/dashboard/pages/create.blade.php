@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')

@endsection



@section('dashboard')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            {{--                        <h1>Create</h1>--}}
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="{{route('pages.index')}}">Pages</a></li>
                                <li class="breadcrumb-item active">Create</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h3 class="card-title">Create</h3>
                                </div>
                                <!-- /.card-header -->
                                <!-- form start -->
                                <form role="form" action="{{route('pages.store')}}" method="POST" enctype="multipart/form-data">
                                    @csrf

                                    @include("backend.components.translate")
                                    <div class="card-body">
                                        <div class="tab-content" id="custom-tabs-two-tabContent">
                                            @foreach($domainsGLOBAL as $key => $item)
                                                <div class="tab-pane fade  @if($key == 0) show active @endif" id="{{$item->lang}}" role="tabpanel" aria-labelledby="custom-tabs-two-{{$item->lang}}-tab">
                                                    <div class="card-body row">
                                                        <div class="form-group col-6">
                                                            <label for="exampleInputPassword1">Sub Title</label>
                                                            <input type="text" class="form-control" name="langs[{{ $item->lang }}][sub_title]" id="exampleInputPassword1" placeholder="Sub Title" value="{{ old('langs['.$item->lang.'][sub_title]') }}"  >
                                                        </div>
                                                        <div class="form-group col-6">
                                                            <label for="exampleInputPassword1">Title</label>
                                                            <input type="text" class="form-control" name="langs[{{ $item->lang }}][title]" id="exampleInputPassword1" placeholder="Title" value="{{ old('langs['.$item->lang.'][title]') }}"  >
                                                        </div>
                                                        <div class="form-group col-12">
                                                            <label for="exampleInputPassword2">Description</label>
                                                            <textarea class="summernote" name="langs[{{ $item->lang }}][description]">{{ old('langs['.$item->lang.'][description]') }}</textarea>
                                                        </div>
                                                        <input type="hidden" style="display: none" name="langs[{{ $item->lang }}][domain_id]" value="{{$item->id}}">
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="custom-add-info-{{$item->lang}}" data-lang="{{ $item->lang }}" ></div>
                                                        <div class="card card-default custom-add-info-button" data-lang="{{ $item->lang }}" data-domain="{{ $item->id }}" data-key="0" style="background-color: #8080804a; color: grey; cursor: pointer">
                                                            <div class="card-header" style="text-align: center;margin: 0 auto;display: block;">
                                                                <h3  class="card-title">Add info</h3>
                                                            </div>
                                                            <!-- /.card-body -->
                                                        </div>
                                                        <!-- /.card -->
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="card-header" style="    border: 1px solid #80808038;background: #0037ff52;color: #0008ff;">
                                        <h3 class="card-title">More</h3>
                                    </div>
                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <label for="">Menu name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" name="menu_name" placeholder="Menu name" value="{{ old('menu_name') }}"  >
                                            </div>
                                        </div>

                                    </div>
                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">First Image/Video</label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image_one_header" id="imgfilesOneHeader">
                                                    <label class="custom-file-label" for="imgfilesOneHeader">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one-header">

                                        </div>
                                    </div>
                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <label for="exampleInputFile">
                                                Image
                                            </label>
                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="image_one" id="imgfilesOne">
                                                    <label class="custom-file-label" for="imgfilesOne">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view-one">

                                        </div>
                                    </div>
                                    <div class="card-body row">
                                        <div class="form-group col-12">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                    <input type="checkbox" class="custom-control-input" id="customSwitch3" name="checked_gallery">
                                                    <label class="custom-control-label" for="customSwitch3">Gallery Images</label>
                                                </div>
                                            </div>
                                            <div class="input-group custom-display-none-checkbox-galleris" style="display: none">
                                                <div class="custom-file">
                                                    <input type="file"  class="custom-file-input" name="images[]" id="imgfiles" multiple>
                                                    <label class="custom-file-label" for="imgfiles">Choose file</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="img-view">

                                        </div>
                                    </div>

                                    <div class="card-body row">
                                        <div class="form-group  col-12">
                                            <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch31" name="popular_destinations">
                                                <label class="custom-control-label" for="customSwitch31">Popular Destinations View</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary story-submit">Save</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card -->
                        </div>
                        <!--/.col (left) -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
@endsection

@section('js')
    <script>
        $(document).on('click', '#customSwitch3', function () {
            if($(this)[0].checked){
                $('.custom-display-none-checkbox-galleris').attr('style', 'display:block')
            }else{
                $('.custom-display-none-checkbox-galleris').attr('style', 'display:none')
            }
        })


        $(document).on('click', '.custom-add-info-button', function () {
            $(function summer () {
                // Summernote
                $('.summernote').summernote({
                    height: 200
                });
                // CodeMirror
                CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                    mode: "htmlmixed",
                    theme: "monokai"
                });
            })
            let key = $(this).attr('data-key')
            let keyplas = (key*1)+1
            $(this).attr('data-key', keyplas)
            let lang = $(this).attr('data-lang');
            let domain = $(this).attr('data-domain');
            console.log(lang)
            console.log(domain)
            $(`.custom-add-info-${lang}`).append(`
                <div class="card card-default collapsed-card">

                        <div class="card-header" >
                            <button type="button" style="color: grey;     width: 98%;" class="btn btn-tool" data-card-widget="collapse">
                                <div class="card-header" >
                                    <h3 class="card-title" style="display: contents">Info</h3>
                                    <h3 class="card-title">${keyplas} (${lang})</h3>
                                </div>
                            </button>
                            <div class="card-tools">
                                <button type="button" style="color: red" class="btn btn-tool custom-remove-info"><i class="fas fa-times"></i></button>
                            </div>
                        </div>

                    <div class="card-body" style="display: none;">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Title</label>
                            <input type="text" class="form-control" name="item[${keyplas}][${lang}][title]" value="">
                        </div>
                        <div class="form-group">
                            <label>Description</label>
                            <textarea class="summernote" name="item[${keyplas}][${lang}][description]"></textarea>
                        </div>
                        <input type="hidden" class="form-control" name="item[${keyplas}][${lang}][domain_id]" value="${domain}">
                    </div>
                </div>
            `);
        })

        $(document).on('click', '.custom-remove-info', function () {
            if (confirm('Are you sure you want to save this thing into the database?')) {
                // Save it!
                $(this).closest('.card-tools').closest('.card-header').closest('.collapsed-card').remove()
            } else {
                // Do nothing!
                console.log('Thing was not saved to the database.');
            }
        })
    </script>

@endsection


