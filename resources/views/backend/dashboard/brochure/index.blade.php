@extends('layouts.dashboard')


@section('title')
    <title>Company</title>
@endsection

@section('css')
    <!-- DataTables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.4/css/buttons.dataTables.min.css">
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Brochures</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('dashboard')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Brochures</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @if(count($brochures) == 0)
                            <div>
                                <a href="{{route('brochure.create')}}" class="btn btn-success" style="float: right; margin: 15px">Create</a>
                            </div>
                            @endif
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Sub Title</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Access</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($brochures as $item)
                                        <tr>
                                            <td style="width: 15%"><img src="{{asset($item->path)}}" alt="" style="width: 100%"></td>
                                            <td>{{ $item->accesser->sub_title }}</td>
                                            <td>{{ $item->accesser->title }}</td>
                                            <td>{!! $item->accesser->description !!}</td>
                                            <td>
                                                <div class="d-flex">
                                                        <a style="padding: 0px 5px;margin: 0px 8px;" href="{{ route("brochure.edit", $item->id) }}" class="btn btn-primary edit-access"><i  class="fas fa-edit" ></i></a>
                                                        <form method="POST" action="{{ route("brochure.destroy" , $item->id) }}">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="javascript:;" onclick="return confirm('Are you sure you want to delete this item?');">
                                                                <button type="submit" style="padding: 0px 5px;" class="ml-2 btn btn-danger" >
                                                                    <i class="fa fa-trash"></i>
                                                                </button>
                                                            </a>
                                                        </form>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@section('js')


@endsection
