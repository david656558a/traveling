@extends('layouts.dashboard')


@section('title')
    <title>Create</title>
@endsection

@section('css')
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/bankCard/dist/css/DatPayment.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/backend/bankCard/example.css')}}">
    <style>
        .password_info{
            color: red;
            font-size: 12px;
            margin-left: 15px;
        }
    </style>
@endsection



@section('dashboard')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Company Create</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/admin">Home</a></li>
                            <li class="breadcrumb-item active">Create</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
            <form action="{{route('domain.store')}}" method="post" id="quickForm">
                @method('POST')
                @csrf
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h3 class="card-title">Domain</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Company name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{old('name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email"  class="form-control @if($errors->any() && in_array('email.unique', $errors->all()))  is-invalid @endif" placeholder="Enter email" value="">
                                            @if($errors->any() && in_array('email.unique', $errors->all()))<span class="error invalid-feedback">Email address is unique</span>  @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="text" name="password" class="form-control" placeholder="Password" >
                                            <p class="password_info">If you don't fill in the password, a randomly generated password will be sent to this e-mail.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Subscription</label>
                                            <select class="form-control select2bs4" name="subscription_id" style="width: 100%;">
                                                    <option value="1">11 Year</option>
                                            </select>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <div class="col-md-6">
                                <div class="card card-default">
                                    <div class="card-header">
                                        <h3 class="card-title">Domain</h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Company name</label>
                                            <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{old('name')}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email"  class="form-control @if($errors->any() && in_array('email.unique', $errors->all()))  is-invalid @endif" placeholder="Enter email" value="">
                                            @if($errors->any() && in_array('email.unique', $errors->all()))<span class="error invalid-feedback">Email address is unique</span>  @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="text" name="password" class="form-control" placeholder="Password" >
                                            <p class="password_info">If you don't fill in the password, a randomly generated password will be sent to this e-mail.</p>
                                        </div>
                                        <div class="form-group">
                                            <label>Subscription</label>
                                            <select class="form-control select2bs4" name="subscription_id" style="width: 100%;">
                                                <option value="1">11 Year</option>
                                            </select>
                                        </div>
                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </div>
                </section>
            </form>
    <!-- /.content -->
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $('#quickForm').validate({
                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    password: {
                        minlength: 5
                    },
                    subscription_id: {
                        required: true
                    },

                },
                messages: {
                    email: {
                        required: "Please enter a email address",
                        email: "Please enter a vaild email address"
                    },
                    cvv: {
                        maxlength: "Please enter a vaild email address",
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
        });
    </script>
@endsection


