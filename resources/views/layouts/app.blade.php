<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('frontend.components.css')
    @yield('css')
    <title>Home Page</title>
</head>
<body>
    @include('frontend.components.header')
    @yield('content')
    @include('frontend.components.footer')


@include('frontend.components.js')
@yield('js')
</body>
</html>
