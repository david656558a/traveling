<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @yield('title')
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @include('backend.components.css')
    @yield('css')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

@auth()
    <!-- Navbar -->
    @include('backend.components.navbar')
    <!-- /.navbar -->

    @include('backend.components.flashMessage')

    <!-- Main Sidebar Container -->
    @include('backend.components.sidebar')

    <!-- Content Wrapper. Contains page content -->
    @yield('dashboard')


   @include('backend.components.footer')

@else
    <script>window.location = "/login";</script>
@endif
   @include('backend.components.js')
   @yield('js')
</div>
<!-- ./wrapper -->

</body>
</html>
