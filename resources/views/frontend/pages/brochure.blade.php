@extends('layouts.app')

@section('content')
    <main class="main mb-5">
        <div class="container_1640">
            <div class="d-flex justify-content-xl-between justify-content-center flex-wrap-reverse">
                <div class="brochuresLeftside">
                    <img src="{{asset($brochure->path)}}" alt="">
                </div>
                <div class="brochureRightside mb-4">
                    <p class="subtitle mb-1">{{$brochure->accesser->sub_title}}</p>
                    <p class="main_title">{{$brochure->accesser->title}}</p>
                    <p class="main_text font_weight700 font_size18">
                        {!! $brochure->accesser->description !!}
                    </p>
                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                        Click to view our brochure
                        <span class="ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                    </span>
                    </a>
                </div>
            </div>

        </div>
    </main>
@endsection
