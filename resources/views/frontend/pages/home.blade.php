@extends('layouts.app')

@section('content')
    <div class="custom-header-div">
        <div class="header-homePage" style=" background: url({{asset($header->path)}}) no-repeat scroll center;"></div>
        @if($news)
        <div class="header header-orangeHeader py-2 custom-info-line-home-page">
            <div class="container_1640 custom-info-head">
                <div class="header-orangeHeader_content justify-content-between align-items-center"> {{--  d-flex  --}}
                    <button class="addBtn p-0 custom-button-header-info" data-view="0">
                        <img src="{{asset('/assets/frontend/image/addBtn.png')}}" alt="" class="custom-plus">
                        <img src="{{asset('/assets/frontend/image/minus.png')}}" alt="" class="custom-minus" style="display: none">
                    </button>
                    <p class="font_style font_weight600 mb-0 custom-hidden-small-text">
                        <span class="font_weight700">{{$news->accesser->title}}:</span>
                        {{$news->accesser->sub_description}}
                    </p>
                </div>
                <div class="custom-hidden-div-info" style="display: none">
                    <span class="font_style font_weight600 mb-0 font_weight700">{{$news->accesser->title}}</span>
                    <p class="font_style font_weight600 mb-0">
                        {!! $news->accesser->description !!}
                    </p>
                </div>
            </div>
        </div>
        @endif
        <div class="  d-flex justify-content-between align-items-center flex-wrap-reverse flex-xl-nowrap  w-100 custom-header-image-home-page">
            <div class="introLeft custom-iframe-head">

            </div>
            <div class="introRight mb-5 custom-text-center-head">
                <p class="font_style_orange font_weight700 mb-0 custom-line-height" >{{$header->accesser->title}}</p>
                <p class="white_title mb-0">{{$header->accesser->description}}</p>
            </div>
        </div>
    </div>



    <main class="main mb-5">
        <div class="container_1640">
            <div class="main_intro mb-5">
                <p class="subtitle mb-1">The most popular destinations in CUBA</p>
                <p class="main_title">book your travel today</p>
                <div class="owl-carousel owl-theme main_introCarousel mt-5">
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                                        </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Cayo Coco, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Cayo Coco, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Cayo Coco, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title"><a href="{{route('hotel.single')}}" class="custom-hotel-single">Havana, Cuba</a></p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                        <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                      </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_blueSection py-5">
            <div class="container_1640 pt-5">
                <p class="subtitle mb-1">Find your ticket to cuba with plane or ship</p>
                <p class="main_title main_title_white ">how to get to cuba</p>
                <p class="d-flex font_style ">
            <span class="me-2">
              <img src="{{asset('/assets/frontend/image/plane.png')}}" alt="">
            </span>
                    Flight Deals
                </p>
                <div class="flight_section justify-content-center d-flex flex-wrap mt-5">
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                    <div class="flight_section-box">
                        <div class="d-flex justify-content-between">
                            <div class="d-flex justify-content-between fligth_section-boxTop">
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                <button class="dirChangeBtn p-0">
                                    <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                </button>
                                <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                            </div>
                            <div class="introCarousel_icon">
                                <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                            </div>
                        </div>
                        <p>
                            Aug 27, 2022 - Sep 9, 2022
                            <span class="ms-4">
                  12:15 - 17:30
                </span>
                        </p>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                <div class="flight_section-boxImg">
                                    <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                </div>
                                <p class="mb-0 font_text_gray">Economy</p>
                            </div>
                            <div>
                                <p class="mb-0 font_text_gray">Total price</p>
                                <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                            </div>
                        </div>
                    </div>
                <!-- <a href="javascript:void(0)" id="show-more" class="seeMore d-flex justify-content-center align-items-center m-auto mt-5">
              See More
              <span class="ms-3">
                <img src="{{asset('/assets/frontend/image/downArrow.png')}}" alt="">
              </span>
            </a> -->
                    <div id="show-more" class="seeMore w-100 my-4">
                        <a href="javascript:void(0)" class="d-flex justify-content-center align-items-center subtitle">
                            See more
                            <span class="ms-3">
                  <img src="{{asset('/assets/frontend/image/downArrow.png')}}" alt="">
                </span>
                        </a>
                    </div>
                </div>
                <div id="show-more-content">
                    <div class="flight_section d-flex flex-wrap mt-4">
                        <div class="flight_section-box">
                            <div class="d-flex justify-content-between">
                                <div class="d-flex justify-content-between fligth_section-boxTop">
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                    <button class="dirChangeBtn p-0">
                                        <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                    </button>
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                                </div>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p>
                                Aug 27, 2022 - Sep 9, 2022
                                <span class="ms-4">
                    12:15 - 17:30
                  </span>
                            </p>
                            <div class="d-flex justify-content-between align-items-end">
                                <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                    <div class="flight_section-boxImg">
                                        <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                    </div>
                                    <p class="mb-0 font_text_gray">Economy</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Total price</p>
                                    <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                                </div>
                            </div>
                        </div>
                        <div class="flight_section-box">
                            <div class="d-flex justify-content-between">
                                <div class="d-flex justify-content-between fligth_section-boxTop">
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                    <button class="dirChangeBtn p-0">
                                        <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                    </button>
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                                </div>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p>
                                Aug 27, 2022 - Sep 9, 2022
                                <span class="ms-4">
                    12:15 - 17:30
                  </span>
                            </p>
                            <div class="d-flex justify-content-between align-items-end">
                                <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                    <div class="flight_section-boxImg">
                                        <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                    </div>
                                    <p class="mb-0 font_text_gray">Economy</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Total price</p>
                                    <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                                </div>
                            </div>
                        </div>
                        <div class="flight_section-box">
                            <div class="d-flex justify-content-between">
                                <div class="d-flex justify-content-between fligth_section-boxTop">
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Toronto</p>
                                    <button class="dirChangeBtn p-0">
                                        <img src="{{asset('/assets/frontend/image/arrow-right-left.png')}}" alt="">
                                    </button>
                                    <p class="general_font subtitle_blue general_font_size20 font_weight700">Havana</p>
                                </div>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p>
                                Aug 27, 2022 - Sep 9, 2022
                                <span class="ms-4">
                    12:15 - 17:30
                  </span>
                            </p>
                            <div class="d-flex justify-content-between align-items-end">
                                <div class="d-flex justify-content-between align-items-end fligth_section-boxTop">
                                    <div class="flight_section-boxImg">
                                        <img src="{{asset('/assets/frontend/image/fligthImg.png')}}" alt="">
                                    </div>
                                    <p class="mb-0 font_text_gray">Economy</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Total price</p>
                                    <p class="mb-0 subtitle general_font_size20 font_weight800">$450</p>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- <a href="javascript:void(0)" id="show-less" class="seeMore d-flex justify-content-center align-items-center m-auto mt-5">
              See More
              <span class="ms-3">
                <img src="{{asset('/assets/frontend/image/downArrow.png')}}" alt="">
              </span>
            </a> -->
                    <div id="show-less" class="seeMore w-100 my-4">
                        <a href="javascript:void(0)" class="d-flex justify-content-center align-items-center subtitle">
                            See less
                            <span class="ms-3">
                <img src="{{asset('/assets/frontend/image/downArrow.png')}}" alt="">
              </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_bottomSection py-5">
            <div class="container_1640">
                <p class="subtitle mb-1">The most popular destinations in CUBA</p>
                <p class="main_title">what can you see in cuba</p>
                @foreach($contents as $key => $content)
                    @if($key % 2)
                        <div class="d-flex justify-content-lg-between justify-content-center align-items-center flex-lg-nowrap flex-wrap-reverse ">
                            <div class="bottomSection_right me-0 me-lg-3">
                                <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize">{{$content->accesser->title}}</p>
                                <p class="main_text">{!! $content->accesser->description !!}</p>
                            </div>
                            <div class="bottomSection_img">
                                <img src="{{asset($content->path)}}" alt="">
                            </div>
                        </div>
                    @else
                        <div class="d-flex justify-content-lg-between justify-content-center align-items-center flex-lg-nowrap flex-wrap mt-5">
                            <div class="bottomSection_img">
                                <img src="{{asset($content->path)}}" alt="">
                            </div>
                            <div class="bottomSection_right ms-0 ms-lg-3">
                                <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize">{{$content->accesser->title}}</p>
                                <p class="main_text">{!! $content->accesser->description !!}</p>
                            </div>
                        </div>
                    @endif
                @endforeach

{{--                <div class="d-flex justify-content-lg-between justify-content-center align-items-center flex-lg-nowrap flex-wrap">--}}
{{--                    <div class="bottomSection_img">--}}
{{--                        <img src="{{asset('/assets/frontend/image/infoImg3.png')}}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="bottomSection_right ms-0 ms-lg-3">--}}
{{--                        <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize">Food</p>--}}
{{--                        <p class="main_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="d-flex justify-content-lg-between justify-content-center align-items-center flex-lg-nowrap flex-wrap-reverse ">--}}
{{--                    <div class="bottomSection_right me-0 me-lg-3">--}}
{{--                        <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize">Activity</p>--}}
{{--                        <p class="main_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>--}}
{{--                    </div>--}}
{{--                    <div class="bottomSection_img">--}}
{{--                        <img src="{{asset('/assets/frontend/image/infoImg4.png')}}" alt="">--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--                <div class="d-flex justify-content-lg-between justify-content-center align-items-center flex-lg-nowrap flex-wrap">--}}
{{--                    <div class="bottomSection_img">--}}
{{--                        <img src="{{asset('/assets/frontend/image/infoImg5.png')}}" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="bottomSection_right ms-0 ms-lg-3">--}}
{{--                        <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize">Activity</p>--}}
{{--                        <p class="main_text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>

        </div>
    </main>
@endsection

@section('js')
    <script defer>
        $('.main_introCarousel').owlCarousel({
            loop:true,
            margin:25,
            nav:true,
            dots:false,
            navText: ["<div class='nav_button_prev owl-prev'> <img src='{{asset('/assets/frontend/image/left.png')}}'></div>", "<div class='nav_button_next owl-next'> <img src='{{asset('/assets/frontend/image/right.png')}}'> </div>"],
            responsive:{
                0:{
                    items:1
                },
                460:{
                    items:1.5
                },
                768:{
                    items:2.5
                },
                1200:{
                    items:3
                }
            }
        })


        $('#show-more-content').hide();

        $('#show-more').click(function(){
            $('#show-more-content').show(200);
            $('#show-less').show();
            $('#show-more').hide();
        });

        $('#show-less').click(function(){
            $('#show-more-content').hide(200);
            $('#show-more').show();
            $(this).hide();
        });
    </script>

    <script>
        $('.custom-hidden-div-info').hide();
        $('.custom-hidden-small-text').show();
        $(document).on('click', '.custom-button-header-info', function () {
            // alert(1);
            if($(this).attr('data-view') == 0){
                $(this).closest('.custom-info-head').closest('.custom-info-line-home-page').addClass("custom-info-line-home-page-view-all-show")
                $(this).closest('.custom-info-head').closest('.custom-info-line-home-page').removeClass("custom-info-line-home-page-view-all-hide")
                $('.custom-hidden-div-info').show(800);
                // $(this).closest('.custom-info-head').closest('.custom-info-line-home-page').attr('style', 'height: 191px;transition-delay: 250ms;transition-property: margin-right;')
                // $('.custom-hidden-div-info').attr('style', 'display: block')
                $(this).attr('data-view', 1)
                $('.custom-hidden-small-text').hide(800);
                $('.custom-button-header-info').attr('style', 'margin-top:  -29px transition-delay: 200ms;\n' +
                    '    transition-property: margin-top;\n' +
                    '    transition-duration: 3s;')
                $('.custom-plus').attr('style', 'display: none;')
                $('.custom-minus').attr('style', 'display: block;')
            }else{
                $(this).closest('.custom-info-head').closest('.custom-info-line-home-page').removeClass("custom-info-line-home-page-view-all-show")
                $(this).closest('.custom-info-head').closest('.custom-info-line-home-page').addClass("custom-info-line-home-page-view-all-hide")
                $('.custom-hidden-div-info').hide(800);
                // $('.custom-hidden-div-info').attr('style', 'display: none')
                $(this).attr('data-view', 0)
                $('.custom-hidden-small-text').show(800);
                $('.custom-button-header-info').attr('style', 'margin-top:  1px;  transition-delay: 100ms;\n' +
                    '    transition-property: margin-top;\n' +
                    '    transition-duration: 1s;')
                $('.custom-plus').attr('style', 'display: block;')
                $('.custom-minus').attr('style', 'display: none;')
            }

        })





    </script>
@endsection

