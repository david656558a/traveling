@extends('layouts.app')

@section('content')
    <div class="header_intro position-relative" onclick="play1();">
        <!-- <div id="iframe_item1" class="iframe_item-video iframe_item1"></div>
        <button class="playBtn">
            <img src="image/youtube.png" alt="">
        </button> -->
{{--        {{dd($extension)}}--}}
        @if(in_array($extension, ['webm', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'ogg', 'mp4', 'm4p', 'm4v', 'avi', 'wmv', 'mov', 'qt', 'flv', 'swf']))
            <video width="100%" height="741" controls>
                <source src="{{asset($page->path_header)}}">
            </video>
        @else
            <div class="custom-header-div">
                <div class="header-homePage" style=" background: url({{asset($page->path_header)}}) no-repeat scroll center;min-height: 747px;"></div>
            </div>
        @endif
    </div>
    <main class="main mb-5">
        <div class="container_1640">
            <div class="singlePage d-flex justify-content-lg-between justify-content-center flex-lg-nowrap flex-wrap mt-5">
                <div class="leftsidePackage">
{{--                    {{dd($page)}}--}}
                    <p class="subtitle mb-1">{{$page->accesser->sub_title}}</p>
                    <p class="main_title">{{$page->accesser->title}}</p>
                    <p class="main_text mt-5">
                       {!! $page->accesser->description !!}
                    </p>
{{--                    {{dd($page)}}--}}
                    @if(!empty($page->items))
                    <div class="accordion accordion-flush mt-5" id="accordionFlushExample">
                        @foreach($page->items as $itemKey => $item)
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="flush-headingOne{{$itemKey}}">
                                    <button class="accordion-button infoAccordionBtn collapsed ps-0 main_text general_font_size24 font_weight700" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne{{$itemKey}}" aria-expanded="false" aria-controls="flush-collapseOne{{$itemKey}}">
                                        {{$item->accesser->title}}
                                    </button>
                                </h2>
                                <div id="flush-collapseOne{{$itemKey}}" class="accordion-collapse collapse" aria-labelledby="flush-headingOne{{$itemKey}}" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">{!! $item->accesser->description !!}</div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endif
                </div>
                <div class="rightsidePackage">
                    <img src="{{asset($page->path)}}" alt="">
                </div>
            </div>
            @if($page->view_gallery)
                <p class="main_title general_font_size36 mt-5 pt-5 ">Gallery</p>
                <div class="owl-carousel owl-theme singlePageCarousel">
                    @foreach($page->images as $img)
                    <div class="item singlePageCarousel-item">
                        <img src="{{asset($img->path)}}" alt="">
                    </div>
                    @endforeach
                </div>
            @endif
            @if($page->view_destination)
                <p class="main_title general_font_size36 mt-5 pt-5">Popular Destinations</p>
                <div class="owl-carousel owl-theme main_introCarousel">
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Cayo Coco, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Cayo Coco, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel2.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel1.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Cayo Coco, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item introCarousel_item">
                        <div class="introCarousel_itemImg">
                            <img src="{{asset('/assets/frontend/image/travelCubaCarousel3.png')}}" alt="">
                        </div>
                        <div class="p-3">
                            <div class="d-flex justify-content-between">
                                <p class="black_title">Havana, Cuba</p>
                                <div class="introCarousel_icon">
                                    <img src="{{asset('/assets/frontend/image/sun.png')}}" alt="">
                                </div>
                            </div>
                            <p class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <div class="d-flex justify-content-between">
                                <div>
                                    <div class="d-flex mb-2">
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                        <span class="me-2"><img src="{{asset('/assets/frontend/image/Star.png')}}" alt=""></span>
                                    </div>
                                    <p class="mb-0 font_text_gray"><del>$750</del></p>
                                    <p class="mb-0 font_style_red">$350</p>
                                </div>
                                <div>
                                    <p class="mb-0 font_text_gray">Aug 27, 2022</p>
                                    <p class="mb-0 font_text_gray">7 days</p>
                                    <a href="#" class="d-flex align-items-baseline font_size18 subtitle">
                                        Details
                                        <span class="detailArrow ms-2">
                            <img src="{{asset('/assets/frontend/image/arrow.png')}}" alt="">
                          </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </main>

@endsection

@section('js')
    <script defer>
        $('.main_introCarousel').owlCarousel({
            loop:true,
            margin:25,
            nav:true,
            dots:false,
            navText: ["<div class='nav_button_prev owl-prev'> <img src='{{asset('/assets/frontend/image/left.png')}}'></div>", "<div class='nav_button_next owl-next'> <img src='{{asset('/assets/frontend/image/right.png')}}'> </div>"],
            responsive:{
                0:{
                    items:1
                },
                460:{
                    items:1.5
                },
                768:{
                    items:2.5
                },
                1200:{
                    items:3
                }
            }
        })

        $('.singlePageCarousel ').owlCarousel({
            loop:true,
            margin:25,
            nav:true,
            dots:false,
            navText: ["<div class='nav_button_prev owl-prev'> <img src='{{asset('/assets/frontend/image/left.png')}}'></div>", "<div class='nav_button_next owl-next'> <img src='{{asset('/assets/frontend/image/right.png')}}'> </div>"],
            responsive:{
                0:{
                    items:1
                },
                340:{
                    items:1.5
                },
                768:{
                    items:2.5
                },
                1200:{
                    items:4
                }
            }
        })
    </script>
@endsection
