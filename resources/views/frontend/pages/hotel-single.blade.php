@extends('layouts.app')

@section('content')
    <main class="main mb-5">
        <div class="container_1640">
            <p class="subtitle mb-1">Varadero</p>
            <p class="main_title">Casa Garcia Dihigo</p>
            <!-- lightbox -->
            <div class="d-flex justify-content-between flex-wrap flex-xl-nowrap hotelSinglePage_lightbox">
                <div class="hotelSinglePage_leftside">
                    <div class="glaryBox d-flex justify-content-xl-between  justify-content-center flex-wrap flex-xl-nowrap mb-5">
                        <div class="leftside-detailsImg">
                            <a data-fancybox="gallery" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                            </a>
                        </div>
                        <div class="rightside-detailsImg d-flex flex-xl-column flex-row">
                            <div class="top-img_right">
                                <a data-fancybox="gallery" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                    <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                                </a>
                            </div>
                            <div class="top-img_right">
                                <a data-fancybox="gallery" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                    <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                                </a>
                            </div>
                            <!-- Button trigger modal -->
                            <div class="see-allBtn">
                                <p class="custom-gallery-view-images">View all images</p>
                                <div style="display: none">
                                    <a data-fancybox="gallery" class="custom-click-view-slider" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                        <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                                    </a>
                                    <a data-fancybox="gallery" class="custom-click-view-slider" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                        <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                                    </a>
                                    <a data-fancybox="gallery" class="custom-click-view-slider" href="{{asset('/assets/frontend/image/brochure.png')}}">
                                        <img src="{{asset('/assets/frontend/image/brochure.png')}}">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="main_text my-5">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </p>
                </div>
                <div class="rightsideSingle position-relative px-4 py-3">
                    <p class="subtitle font_weight700">Varadero</p>
                    <div class="owl-carousel owl-theme hotel_info_carousel">
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Hotels
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Cars
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Food
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Tours
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Hotels
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Cars
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Food
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Tours
                        </div>
                    </div>
                    <div class="reviewSection">
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Casa Garcia Dihigo</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Casa perla</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="general_font general_font_size36 font_weight800 subtitle_blue text-capitalize mb-4 mt-5">
                Facilities
            </p>
            <div class="row g-2">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/wi-fi-logo.png')}}" alt="" class="facility_img">
                    Free WiFi
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/breakfast.png')}}" alt="" class="facility_img">
                    Breakfast
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/ic_baseline-pets.png')}}" alt="" class="facility_img">
                    Pet friendly
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/hairdryer-silhouette-side-view.png')}}" alt="" class="facility_img">
                    Hairdryer
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/fa6-solid_square-parking.png')}}" alt="" class="facility_img">
                    Free parking
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/cocktail 1.png')}}" alt="" class="facility_img">
                    Bar
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/covered-food-tray-on-a-hand-of-hotel-room-service 1.png')}}" alt="" class="facility_img">
                    Room service
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/refrigerator.png')}}" alt="" class="facility_img">
                    Minifridge
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/satellite-dish.png')}}" alt="" class="facility_img">
                    Satellite TV
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/Vector.png')}}" alt="" class="facility_img">
                    Balcony
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/ornagePhone.png')}}" alt="" class="facility_img">
                    Telephone
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/facial-treatment 1.png')}}" alt="" class="facility_img">
                    Full European Spa
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/safe-box.png')}}" alt="" class="facility_img">
                    Safety deposit box
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/socket 1.png')}}" alt="" class="facility_img">
                    110/220 voltage
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/exercise 1.png')}}" alt="" class="facility_img">
                    Fitness centre
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <img src="{{asset('/assets/frontend/image/noto_woman-dancing-medium-light-skin-tone.png')}}" alt="" class="facility_img">
                    Night club
                </div>
            </div>
            <p class="general_font general_font_size36 font_weight800 subtitle_blue text-capitalize mb-4 mt-5">
                All-Inclusive
            </p>
            <ul class="listStyleBlue p-0 mb-5">
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed d
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit ame
                </li>
                <li class="font_text_gray">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                </li>
            </ul>
        </div>
    </main>
@endsection
@section('js')
    <script defer>
        $('.hotel_info_carousel').owlCarousel({
            loop:true,
            margin:25,
            nav:true,
            dots:false,
            navText: ["<div class='navBtnPrev owl-prev'>", "<div class='navBtnNext owl-next'> <img src='{{asset('/assets/frontend/image/chevron-right.png')}}'> </div>"],
            responsive:{
                0:{
                    items:1
                },
                768:{
                    items:2
                },
                1200:{
                    items:4.8
                }
            }
        })



        // function showSlides(n) {
        //     var i;
        //     var slides = document.getElementsByClassName("my-slides");
        //     var dots = document.getElementsByClassName("demo-img");
        //
        //     if (n > slides.length) {slide_index = 1}
        //     if (n < 1) {slide_index = slides.length}
        //     for (i = 0; i < slides.length; i++) {
        //         slides[i].style.display = "none";
        //     }
        //     for (i = 0; i < dots.length; i++) {
        //         dots[i].className = dots[i].className.replace(" active", "");
        //     }
        //     slides[slide_index-1].style.display = "block";
        //     dots[slide_index-1].className += " active";
        //     captionText.innerHTML = dots[slide_index-1].alt;
        //     // for (let i = 0; i < imgLengts.childNodes.length; i++) {
        //     //   if (imgLengts.childNodes[i]>=6) {
        //     //     imgLengts.childNodes[i].style.display='none'
        //     //   }
        //     // }
        // }
    </script>
    <script>
        document.addEventListener('DOMContentLoaded',() => {
            let Pbutton = document.querySelector('.custom-gallery-view-images')
            Pbutton.addEventListener('click',function () {
                let a = document.querySelector('.custom-click-view-slider')
                console.log(a)
                a.click()
            })
        })
    </script>
@endsection
