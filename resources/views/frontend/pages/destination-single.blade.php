@extends('layouts.app')

@section('content')
    <div class="custom-header-div">
        <div class="header-homePage" style=" background: url({{asset($destination->path)}}) no-repeat scroll center;min-height: 747px;"></div>
    </div>
    <main class="main mb-5">
        <div class="container_1640">
            <div class="singlePage d-flex justify-content-between flex-wrap flex-xl-nowrap mt-5">
                <div class="leftsideSingle">
                    <p class="subtitle mb-1">{{$destination->accesser->sub_title}}</p>
                    <p class="main_title">{{$destination->accesser->title}}</p>
                    <p class="main_text">
                        {!! $destination->accesser->description !!}
                    </p>
{{--                    <p class="general_font general_font_size20 font_weight800 subtitle_blue text-capitalize mb-1">--}}
{{--                        Exciting things to see and do--}}
{{--                    </p>--}}
{{--                    <ul class="listStyleBlue p-0 mb-5">--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod</li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do</li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet</li>--}}
{{--                        <li class="font_text_gray">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor </li>--}}
{{--                    </ul>--}}
                </div>
                <div class="rightsideSingle position-relative px-4 py-3">
                    <p class="subtitle font_weight700">Varadero</p>
                    <div class="owl-carousel owl-theme hotel_info_carousel ">
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Hotels
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Cars
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Food
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Tours
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Hotels
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Cars
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Food
                        </div>
                        <div class="item hotel_info_carousel-item main_text main_title_white font_weight700">
                            Tours
                        </div>
                    </div>
                    <!-- <div class="rightsideShadow"></div> -->
                    <div class="reviewSection">
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Casa Garcia Dihigo</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Casa perla</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                        <div class="reviewSection_info d-flex justify-content-between my-3">
                            <p class="font_style mb-0">Milagros Renta</p>
                            <div class="reviewSection_star">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                                <img src="{{asset('/assets/frontend/image/Star.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="owl-carousel owl-theme singlePageCarousel main_introCarousel  mt-5 pt-5 ">
                @foreach($destination->images as $img)
                <div class="item singlePageCarousel-item">
                    <img src="{{asset($img->path)}}" alt="">
                </div>
                @endforeach
            </div>
        </div>
    </main>
@endsection
