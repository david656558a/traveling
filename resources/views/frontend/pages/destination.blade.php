@extends('layouts.app')

@section('content')
    <main class="main mb-5">
        <div class="container_1640">
            <div class="main_intro mb-5">
                <p class="subtitle mb-1">The most popular destinations in CUBA</p>
                <p class="main_title">book your travel today</p>
                <div class="destination_section d-flex justify-content-center flex-wrap mt-5">
                    @foreach($destinations as $destination)
                        <a class="destination_section-box" href="{{route('destination.single', $destination->id)}}">
                            <img src="{{asset($destination->path)}}" alt="">
                            <span class="font_style general_font_size20 font_weight800 position-absolute p-3 backdrop_filter w-100 mb-0">
                                {{$destination->accesser->title}}
                            </span>
                        </a>
                    @endforeach

                </div>
            </div>
        </div>
    </main>
@endsection
