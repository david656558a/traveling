<header class="header custom-nav-bar">
    <div class="header-pageHeader">
        <div class="container_1640">
            <nav class="navbar navbar-expand-xxl navbar-light header_navbar">
                <div class="container-fluid p-0">
                    <a class="navbar-brand" href="{{route('home')}}">
                        <img src="{{asset('/assets/frontend/image/logo.png')}}" alt="">
                    </a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav header_navbarNav ms-auto mb-2 mb-lg-0">
{{--                            <li class="nav-item me-5">--}}
{{--                                <a class="nav-link font_style active" aria-current="page" href="#">Deals</a>--}}
{{--                            </li>--}}
                            <li class="nav-item me-5">
                                <a class="nav-link font_style active" aria-current="page" href="{{route('brochure')}}">Brochure</a>
                            </li>
{{--                            @foreach($menuNameGLOBAL as $menuName)--}}
{{--                                <li class="nav-item me-5">--}}
{{--                                    <a class="nav-link font_style" aria-current="page" href="{{route('page', $menuName->id)}}">{{$menuName->menu_name}}</a>--}}
{{--                                </li>--}}
{{--                            @endforeach--}}
{{--                            <li class="nav-item me-5">--}}
{{--                                <a class="nav-link font_style" href="#">FAQ</a>--}}
{{--                            </li>--}}
                            <li class="nav-item me-5">
                                <a class="nav-link font_style" href="{{route('destination')}}">Destination</a>
                            </li>

                            <li class="nav-item position-relative dropdown dropdown_menu me-5">
                                <a class="nav-link font_style dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Destinations
                                </a>
                                <ul class="dropdown-menu parentMenu">
                                    @foreach($menuNameGLOBAL as $menuName)
                                    <li class="dropdown-submenu d-flex justify-content-between align-items-center" data-item="0">
                                        <a class="test d-flex justify-content-between w-100" href="{{route('page', $menuName->id)}}" style="color: white">{{$menuName->menu_name}}</a>
                                    </li>
                                    @endforeach
                                    <li class="dropdown-submenu d-flex justify-content-between align-items-center" data-item="1">
                                        <a class="test d-flex justify-content-between w-100" tabindex="-1" href="#" style="color: white">
                                            menu
                                            <span class="caret">
                                                <img src="{{asset('assets/frontend/image/arrowRight.png')}}" alt="">
                                            </span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-tab dropdownMenu" data-item="1" style="top: 0;height: -webkit-fill-available;">
                                            <li><a tabindex="-1" href="#">1nd level dropdown</a></li>
                                            <li><a tabindex="-1" href="#">1nd level dropdown</a></li>
                                        </ul>
                                    </li>
                                        <li class="dropdown-submenu d-flex justify-content-between align-items-center" data-item="2">
                                            <a class="test d-flex justify-content-between w-100" tabindex="-1" href="#" style="color: white">
                                                menu
                                                <span class="caret">
                                                <img src="{{asset('assets/frontend/image/arrowRight.png')}}" alt="">
                                            </span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-tab dropdownMenu" data-item="2" style="top: 0;height: -webkit-fill-available;">
                                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                                <li><a tabindex="-1" href="#">2nd level dropdown</a></li>
                                            </ul>
                                        </li>
                                        <li class="dropdown-submenu d-flex justify-content-between align-items-center" data-item="3">
                                            <a class="test d-flex justify-content-between w-100" tabindex="-1" href="#" style="color: white">
                                                menu
                                                <span class="caret">
                                                <img src="{{asset('assets/frontend/image/arrowRight.png')}}" alt="">
                                            </span>
                                            </a>
                                            <ul class="dropdown-menu dropdown-menu-tab dropdownMenu" data-item="3" style="top: 0;height: -webkit-fill-available;">
                                                <li><a tabindex="-1" href="#">3nd level dropdown</a></li>
                                                <li><a tabindex="-1" href="#">3nd level dropdown</a></li>
                                            </ul>
                                        </li>
                                </ul>
                            </li>

{{--                            <li class="nav-item dropdown me-5">--}}
{{--                                <a class="nav-link font_style dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">--}}
{{--                                    Destinations--}}
{{--                                </a>--}}
{{--                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
{{--                                    @foreach($menuNameGLOBAL as $menuName)--}}
{{--                                        <li><a class="dropdown-item  font_style" href="{{route('page', $menuName->id)}}">{{$menuName->menu_name}}</a>--}}
{{--                                    @endforeach--}}
{{--                                    <li><a class="dropdown-item  font_style" href="#">Action</a></li>--}}
{{--                                    <li><a class="dropdown-item font_style" href="#">Another action</a></li>--}}
{{--                                    <li><hr class="dropdown-divider"></li>--}}
{{--                                    <li><a class="dropdown-item font_style" href="#">Something else here</a></li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
                        </ul>
                        <ul class="navbar-nav header_navbarNav ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link font_style border_right pe-3" href="tel:{{$infoGLOBAL->phone}}">
                                    {{$infoGLOBAL->phone}}
                                    <span>
                                    <img src="{{asset('/assets/frontend/image/phone.png')}}" alt="">
                                </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link border_right px-3" href="mailto:{{$infoGLOBAL->email}}"><img src="{{asset('/assets/frontend/image/emailIcon.png')}}" alt=""></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link border_right px-3" href="{{$infoGLOBAL->facebook}}"><img src="{{asset('/assets/frontend/image/facebookIcon.png')}}" alt=""></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link border_right px-3" href="{{$infoGLOBAL->instagram}}"><img src="{{asset('/assets/frontend/image/instaIcon.png')}}" alt=""></a>
                            </li>
                            <li class="nav-item d-flex justify-content-center align-items-baseline">
                                @foreach($domainsGLOBAL as $domKey => $domain)
                                    <a class="nav-link font_style" href="https://{{$domain->domain}}/{{Request::decodedPath()}}">{{$domain->name}}</a>
                                    @if(count($domainsGLOBAL)-1 != $domKey)
                                        <span class="font_style">/</span>
                                    @endif
                                @endforeach
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
{{--{{dd(Request::instance())}}--}}
{{--{{dd(Request::route())}}--}}

