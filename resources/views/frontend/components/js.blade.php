<script src="{{asset('/assets/frontend/js/jquery.min.js')}}"></script>
<script src="{{asset('/assets/frontend/js/bootstrapJS/bootstrap.min.js')}}"></script>
<script src="{{asset('/assets/frontend/js/owl.carousel.min.js')}}"></script>
{{--<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>


<script>
    let liElements = document.querySelectorAll('.parentMenu .dropdown-submenu')
    console.log(liElements);
    liElements.forEach(liItem => {
        liItem.addEventListener('click',function(event) {
            // event.preventDefault()
            event.stopPropagation()
            let ulItems = document.querySelectorAll('.dropdownMenu')
            ulItems.forEach(elem => {
                if(elem.dataset.item === this.dataset.item) {
                    if(elem.style.display === 'block') {
                        return elem.style.display = 'none'
                    }
                    elem.style.display = 'block'
                }
                else {
                    elem.style.display = 'none'
                }
            })
        })
    })
</script>
