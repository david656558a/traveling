<footer class="footer">
    <div class="container_1640 py-5">
        <div class="d-flex justify-content-lg-between justify-content-start flex-wrap">
            <div class="footer_leftside">
                <a class="logo" href="#">
                    <img src="{{asset('/assets/frontend/image/logo.png')}}" alt="">
                </a>
                <p class="font_style">
                    {!! $footerGLOBAL->accesser->description !!}
                </p>
            </div>
{{--            <div class="footer_link d-flex flex-column me-2">--}}
{{--                <a href="#" class="font_style my-1">Deals</a>--}}
{{--                <a href="#" class="font_style my-1">FAQ</a>--}}
{{--                <a href="#" class="font_style my-1">Package info</a>--}}
{{--                <a href="#" class="font_style my-1">Destinations</a>--}}
{{--            </div>--}}
{{--            <div class="footer_link d-flex flex-column me-2">--}}
{{--                <a href="#" class="font_style my-1">Hotels</a>--}}
{{--                <a href="#" class="font_style my-1">Flight</a>--}}
{{--                <a href="#" class="font_style my-1">Activity</a>--}}
{{--                <a href="#" class="font_style my-1">Restorants</a>--}}
{{--            </div>--}}
            <div class="footer_link d-flex flex-column me-2">
            @foreach($menuNameGLOBAL as $keyMenu => $menuName)
{{--                @if($keyMenu == 0)--}}
{{--                    --}}
{{--                @endif--}}
{{--                    @if($keyMenu == 2){{dd(in_array($keyMenu, [0, 4, 8, 12, 16, 20]))}}@endif--}}

                    <a href="{{route('page', $menuName->id)}}" class="font_style my-1">{{$menuName->menu_name}}</a>
{{--                @if($keyMenu == 0)--}}
{{--                   --}}
{{--                @endif--}}
            @endforeach
            </div>



            <div class="footer_rightside">
                <div class="d-flex justify-content-between icon_section mb-3">
                    <a href="{{$infoGLOBAL->youtube}}" class="border_right" style="padding: 5px 15px 0px 0px">
                        <img src="{{asset('/assets/frontend/image/logo-youtube.png')}}" alt="">
                    </a>
                    <a href="{{$infoGLOBAL->twitter}}" class="border_right" style="padding: 5px 15px">
                        <img src="{{asset('/assets/frontend/image/icon-twitter.png')}}" alt="">
                    </a>
                    <a href="{{$infoGLOBAL->facebook}}" class="border_right" style="padding: 5px 15px">
                        <img src="{{asset('/assets/frontend/image/facebookIcon.png')}}" alt="">
                    </a>
                    <a href="{{$infoGLOBAL->instagram}}" class="border_right" style="padding: 5px 15px">
                        <img src="{{asset('/assets/frontend/image/instaIcon.png')}}" alt="">
                    </a>
                    <div class="nav-item d-flex justify-content-center align-items-baseline">
                        @foreach($domainsGLOBAL as $domKey => $domain)
                            <a class="nav-link font_style" href="https://{{$domain->domain}}/{{Request::decodedPath()}}">{{$domain->name}}</a>
                            @if(count($domainsGLOBAL)-1 != $domKey)
                                <span class="font_style">/</span>
                            @endif
                        @endforeach
                    </div>
                </div>
                <p class="font_style font_weight600">Contact  us</p>
                <div class="d-flex">
                    <div class="footer_icon me-3">
                        <img src="{{asset('/assets/frontend/image/phone.png')}}" alt="">
                    </div>
                    <p class="font_style general_font_size20 font_weight700 ">
                        {{$infoGLOBAL->phone}}
                    </p>
                </div>
                <div class="d-flex">
                    <div class="footer_icon me-3">
                        <img src="{{asset('/assets/frontend/image/time.png')}}" alt="">
                    </div>
                    <p class="font_style">
                        {{$infoGLOBAL->time}}
                    </p>
                </div>
                <div class="d-flex">
                    <div class="footer_icon me-3">
                        <img src="{{asset('/assets/frontend/image/emailIcon.png')}}" alt="">
                    </div>
                    <p class="font_style">
                        {{$infoGLOBAL->email}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_copyRight d-flex justify-content-center align-items-center py-2">
        <p class="mb-0 font_style">
            {{$footerGLOBAL->accesser->down_footer_one}}
        </p>
        <pre> </pre>
        <span>
          <img src="{{asset($footerGLOBAL->path)}}" alt="" style="width: 50px;">
{{--          <img src="{{asset('/assets/frontend/image/footerCP.png')}}" alt="" style="width: 50px;">--}}
        </span>
        <pre> </pre>
        <p class="mb-0 font_style">
            {{$footerGLOBAL->accesser->down_footer_two}}
        </p>
    </div>
</footer>
