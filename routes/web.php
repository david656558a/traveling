<?php

//use App\Http\Controllers\frontend\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

// todo                                      MESSAGE
//  Everything is being done through Cache, to know on which URL are users and what language should be.


//Route::middleware('domain')->get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// ========================================= Back End ========================================= //
Route::group(['prefix' => '/admin', 'namespace' => 'App\Http\Controllers\Backend', 'middleware'=> 'auth'], function() {

    Route::get('/', 'DashboardController@index')->name('dashboard');

    Route::resource('/domain', DomainController::class);
    Route::resource('/header', HeaderController::class);
    Route::resource('/info', InfoController::class);
    Route::resource('/news', NewsController::class);
    Route::resource('/destination', DestinationController::class);
        Route::post('/destination/delete/image/{id}', 'DestinationController@deleteOne')->name('delete-one-picture-gallery');
    Route::resource('/footer', FooterController::class);
    Route::resource('/content', ContentController::class);
    Route::resource('/pages', PagesController::class);
        Route::post('/pages/delete/image/{id}', 'PagesController@deleteOne')->name('delete-one-picture-gallery-pages');
    Route::resource('/brochure', BrochuresController::class);

});
// --------------------------------------------------------------------------------------------- //





// ========================================= Front End ========================================= //
Route::group([], function() { //'prefix' => '/admin', 'middleware'=> ['domain']
    Route::get('/', function (){ return redirect()->route('home');} );
    Route::get('/home', [App\Http\Controllers\Frontend\HomeController::class, 'home'])->name('home');
    Route::get('/brochure', [App\Http\Controllers\Frontend\HomeController::class, 'brochure'])->name('brochure');
    Route::get('/page/{id}', [App\Http\Controllers\Frontend\HomeController::class, 'page'])->name('page');
    Route::get('/destination', [App\Http\Controllers\Frontend\HomeController::class, 'destination'])->name('destination');
    Route::get('/destination/{id}', [App\Http\Controllers\Frontend\HomeController::class, 'destinationSingle'])->name('destination.single');
    Route::get('/hotel', [App\Http\Controllers\Frontend\HomeController::class, 'hotelSingle'])->name('hotel.single');

});
// --------------------------------------------------------------------------------------------- //




Route::get('/code/run/{type}', function ($type){
//    dd($type);
    $command = '';
    if ($type == 'fresh--seed' || $type == '1'){
        $command = 'migrate:fresh --seed';
        $message = 'fresh --seed [success]';
    }
    elseif ($type == 'migrate' || $type == '2'){
        $command = 'migrate';
        $message = 'migrate [success]';
    }
    elseif ($type == 'storagelink' || $type == '3'){
        $command = 'storage:link';
        $message = 'storage:link [success]';
    }
    elseif ($type == 'job' || $type == '4'){
        $command = 'queue:work';
        $message = 'job run  [success]';
    }
    elseif ($type == 'clear' || $type == '5'){
        $message = 'clear  [success]';
        Artisan::call('config:clear');
        Artisan::call('route:clear');
        Artisan::call('view:clear');
        Artisan::call('event:clear');
        Artisan::call('cache:clear');
        return dd('dun '.$message);
    }
    else{
        echo 'commands: fresh--seed, migrate, storagelink';
        dd('error: no this command please add');
    }

    $artisan = Artisan::call($command);
    return dd('dun '.$message.' |  artisan command: '.$artisan);
});
