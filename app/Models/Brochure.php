<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brochure extends LocalizedModel
{
    use HasFactory;

    protected $table ='brochures';
    protected $fillable = [
        'path',
    ];


}
