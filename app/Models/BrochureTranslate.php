<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BrochureTranslate extends Model
{
    use HasFactory;

    protected $table ='brochure_translates';
    protected $fillable=[
        'domain_id',
        'brochure_id',
        'lang',
        'sub_title',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function brochure()
    {
        return $this->belongsTo(Brochure::class);
    }

}
