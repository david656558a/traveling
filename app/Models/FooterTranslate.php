<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FooterTranslate extends Model
{
    use HasFactory;

    protected $table ='footer_translates';
    protected $fillable=[
        'domain_id',
        'footer_id',
        'lang',
        'description',
        'down_footer_one',
        'down_footer_two',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function footer()
    {
        return $this->belongsTo(Footer::class);
    }
}
