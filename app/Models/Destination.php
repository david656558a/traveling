<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Destination extends LocalizedModel
{
    use HasFactory;

    protected $table ='destinations';
    protected $fillable = [
        'path',
    ];

    public function images()
    {
        return $this->hasMany(DestinationImage::class,'destination_id','id');
    }


}
