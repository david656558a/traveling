<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalizedModel extends Model
{
    use HasFactory;

    public function localizations()
    {
        return $this->hasMany($this->getClassName());
    }

    public function getClassName()
    {
        return get_class($this) . 'Translate';
    }

    public function getAccesserAttribute()
    {
        return DomainHellper::attribute($this);
    }

    public function translateLang()
    {
        // для того чтобы елси был PageItem стал page_item
        $str = lcfirst(class_basename($this));
        $str = preg_replace("/[A-Z]/", '_'.'$0', $str);
        $translateModel = get_class($this).'Translate';
        $lowercase = strtolower($str);
        return $this->hasMany($translateModel,$lowercase.'_id','id');
    }

}
