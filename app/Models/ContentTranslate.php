<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTranslate extends Model
{
    use HasFactory;
    protected $table ='content_translates';
    protected $fillable=[
        'domain_id',
        'content_id',
        'lang',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function destination()
    {
        return $this->belongsTo(Content::class);
    }
}
