<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HeaderTranslate extends Model
{
    use HasFactory;

    protected $table='header_translates';

    protected $fillable=[
        'domain_id',
        'header_id',
        'lang',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function header()
    {
        return $this->belongsTo(Header::class);
    }
}
