<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Header extends LocalizedModel
{
    use HasFactory;

    protected $table='headers';
    protected $fillable=[
        'path',
    ];

}
