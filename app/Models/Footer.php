<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Footer extends LocalizedModel
{
    use HasFactory;

    protected $table='footers';
    protected $fillable=[
        'path',
    ];

}
