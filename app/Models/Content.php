<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends LocalizedModel
{
    use HasFactory;

    protected $table ='contents';
    protected $fillable = [
        'path',
    ];


}
