<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;

    protected $table='test';
    protected $fillable=[
        'domain_id',
        'title',
        'description',
        ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

}
