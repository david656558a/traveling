<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageItem extends LocalizedModel
{
    use HasFactory;

    protected $table='page_items';
    protected $fillable=[
        'page_id',
    ];


    public function page()
    {
        return $this->belongsTo(PageImage::class);
    }
}
