<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    use HasFactory;

    protected $table = "domains";

    protected $fillable = [
        'domain',
        'name',
        'lang',
    ];

    public function test()
    {
        return $this->hasOne(Test::class,'domain_id','id');
    }

    public function header()
    {
        return $this->hasMany(HeaderTranslate::class,'domain_id','id');
    }
}
