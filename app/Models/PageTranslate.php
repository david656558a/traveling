<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageTranslate extends Model
{
    use HasFactory;


    protected $table='page_translates';
    protected $fillable=[
        'domain_id',
        'page_id',
        'lang',
        'sub_title',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}
