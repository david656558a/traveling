<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsTranslate extends Model
{
    use HasFactory;

    protected $table='news_translates';
    protected $fillable=[
        'domain_id',
        'news_id',
        'lang',
        'title',
        'sub_description',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
