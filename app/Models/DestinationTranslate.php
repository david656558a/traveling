<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DestinationTranslate extends Model
{
    use HasFactory;

    protected $table ='destination_translates';
    protected $fillable=[
        'domain_id',
        'destination_id',
        'lang',
        'sub_title',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function destination()
    {
        return $this->belongsTo(Destination::class);
    }
}
