<?php

namespace App\Models;

use App\MyHellper\DomainHellper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends LocalizedModel
{
    use HasFactory;

    protected $table='pages';
    protected $fillable=[
        'menu_name',
        'path',
        'path_header',
        'view_gallery',
        'view_destination',
    ];

    public function images()
    {
        return $this->hasMany(PageImage::class,'page_id','id');
    }

    public function items()
    {
        return $this->hasMany(PageItem::class,'page_id','id');
    }
}
