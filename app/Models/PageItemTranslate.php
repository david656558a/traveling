<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PageItemTranslate extends Model
{
    use HasFactory;

    protected $table='page_item_translates';
    protected $fillable=[
        'domain_id',
        'page_id',
        'page_item_id',
        'lang',
        'title',
        'description',
    ];


    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }


    public function page()
    {
        return $this->belongsTo(Page::class);
    }
    public function item()
    {
        return $this->belongsTo(PageItem::class);
    }
}
