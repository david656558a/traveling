<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Destination\CreateRequest;
use App\Http\Requests\Destination\EditRequest;
use App\Models\Destination;
use App\Models\DestinationImage;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class DestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $destinations = Destination::all();
        return view('backend.dashboard.destination.index', compact('destinations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.destination.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
//        dd($request->all());
        DB::beginTransaction();
        $filepicture = $request['image_one'];
        $firstfile = HellperFiles::FileOne($filepicture, null, 'Destination');
        $pathOne = HellperFiles::PathFile($firstfile);
        $destination = new Destination();
        $destination->path = $pathOne ;
        $destination->save();

        $logofile = HellperFiles::FilesMany($request['image'], null, 'Destination');
        foreach ($logofile as $image){
            $destinationImages = new DestinationImage();
            $destinationImages->path = $image;
            $destinationImages->destination_id = $destination->id;
            $destinationImages->save();
        }

        foreach ($request->langs as $key => $lang){
            $destination->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'sub_title' => $lang['sub_title'] ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('destination.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $destination = Destination::findorFail($id)->load('translateLang');
        return view('backend.dashboard.destination.edit', compact('destination'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        $destination = Destination::findorFail($id);
        DB::beginTransaction();
        if ($request['image_one']){
            $filepicture = $request['image_one'];
            HellperFiles::FilesOneDelete($destination->path);
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Destination');
            $pathOne = HellperFiles::PathFile($firstfile);
            $destination->path = $pathOne ;
        }
        $deleteResult = $destination->localizations()->delete();
        if (!$deleteResult) {
            DB::rollBack();
            return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
        }
        $destination->save();
        if ($request['image']){
            $logofile = HellperFiles::FilesMany($request['image'], null, 'Destination');
            foreach ($logofile as $image){
                $destinationImages = new DestinationImage();
                $destinationImages->path = $image;
                $destinationImages->destination_id = $destination->id;
                $destinationImages->save();
            }
        }
        //ADDING elements in ProductTr
        foreach ($request->langs as $key => $lang){
            $destination->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'sub_title' => $lang['sub_title'] ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('destination.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $destination = Destination::findorFail($id);
        HellperFiles::FilesOneDelete($destination->path);
        $destination->delete();
        $destination->localizations()->delete();
        return redirect()->route('destination.index')->with(['message' => 'Deleted.']);
    }

    public function deleteOne(Request $request)
    {
        $id = $request->id;
        $gallery = DestinationImage::findOrFail($id);
        $productImage = str_replace('/storage', '', $gallery->path);
        Storage::delete('/public' . $productImage);
        $gallery->delete();
        return response()->json($gallery,200);
    }



}
