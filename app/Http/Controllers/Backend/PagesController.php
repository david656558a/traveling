<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Pages\CreateRequest;
use App\Http\Requests\Pages\EditRequest;
use App\Models\Page;
use App\Models\PageImage;
use App\Models\PageItem;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $pages = Page::all();
        return view('backend.dashboard.pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     */
    public function store(CreateRequest $request)
    {
//        dd($request->all());
        try {
            DB::beginTransaction();

            $filepictureHeader = $request['image_one_header'];
            $firstfileHeader = HellperFiles::FileOne($filepictureHeader, null, 'Pages', 'Header');
            $pathOneHeader = HellperFiles::PathFile($firstfileHeader);


            $filepicture = $request['image_one'];
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Pages', 'Right');
            $pathOne = HellperFiles::PathFile($firstfile);

            $page = new Page();
            $page->menu_name = $request->menu_name;
            $page->path = $pathOne ;
            $page->path_header = $pathOneHeader ;
            $page->view_gallery = isset($request->checked_gallery) ? 1 : 0 ;
            $page->view_destination = isset($request->popular_destinations) ? 1 : 0 ;
            $page->save();
            $langName = [];
            foreach ($request->langs as $key => $lang){
                $page->localizations()->create([
                    'domain_id' => $lang['domain_id'] ,
                    'lang' => $key ,
                    'sub_title' => $lang['sub_title'] ,
                    'title' => $lang['title'] ,
                    'description' => $lang['description'] ,
                ]);
                $langName[$key]['lang'] = $key;
                $langName[$key]['domain_id'] = $lang['domain_id'] ;
            }


            if (isset($request->checked_gallery)){
                $filepictureMany = $request['images'];
                $firstfileMany = HellperFiles::FilesMany($filepictureMany, null, 'Pages', 'Gallery');
                foreach ($firstfileMany as $img){
                    $gallery = new PageImage();
                    $gallery->page_id = $page->id;
                    $gallery->path = $img;
                    $gallery->save();
                }
            }



            if(isset($request->item)){
                foreach ($request->item as $value){
                    $item = new PageItem();
                    $item->page_id = $page->id;
                    $item->save();
                    foreach ($value as $index => $lang) {
                        $item->localizations()->create([
                            'domain_id' => $lang['domain_id'],
                            'page_id' => $page->id,
                            'page_item_id' => $item->id,
                            'lang' => $index,
                            'title' => $lang['title'],
                            'description' => $lang['description'],
                        ]);
                    }
                }
            }

            DB::commit();
            return redirect()->route('pages.index')->with('message', 'Successfully');
        }catch (\Exception $q){
            DB::rollBack();
            return [
                'message' => 'error',
                'e' => $q,
                'es' => $q->getCode(),
                'ess' => $q->getMessage(),
            ];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id)->load('images', 'items');
        return view('backend.dashboard.pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditRequest $request, $id)
    {
//        dd($request->all());
        try {
            DB::beginTransaction();
            $page = Page::findOrFail($id);


            $filepictureHeader = $request['image_one_header'];
            if ($filepictureHeader) {
                $productImage = str_replace('/storage', '', $page->path_header);
                Storage::delete('/public' . $productImage);
                $firstfileHeader = HellperFiles::FileOne($filepictureHeader, null, 'Pages', 'Header');
                $pathOneHeader = HellperFiles::PathFile($firstfileHeader);
                $page->path_header = $pathOneHeader ;
            }

            $filepicture = $request['image_one'];
            if ($filepicture){
                $productImage = str_replace('/storage', '', $page->path);
                Storage::delete('/public' . $productImage);
                $firstfile = HellperFiles::FileOne($filepicture, null, 'Pages', 'Right');
                $pathOne = HellperFiles::PathFile($firstfile);
                $page->path = $pathOne ;
            }

            $page->menu_name = $request->menu_name;
            $page->view_gallery = isset($request->checked_gallery) ? 1 : 0 ;
            $page->view_destination = isset($request->popular_destinations) ? 1 : 0 ;
            $page->save();


            $deleteResult = $page->localizations()->delete();
            if (!$deleteResult) {
                DB::rollBack();
                return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
            }
            $langName = [];
            foreach ($request->langs as $key => $lang){
                $page->localizations()->create([
                    'domain_id' => $lang['domain_id'] ,
                    'lang' => $key ,
                    'sub_title' => $lang['sub_title'] ,
                    'title' => $lang['title'] ,
                    'description' => $lang['description'] ,
                ]);
                $langName[$key]['lang'] = $key;
                $langName[$key]['domain_id'] = $lang['domain_id'] ;
            }


            $filepictureMany = $request['images'];
            if (isset($filepictureMany) && isset($request->checked_gallery)){
                $firstfileMany = HellperFiles::FilesMany($filepictureMany, null, 'Pages', 'Gallery');
                foreach ($firstfileMany as $img){
                    $gallery = new PageImage();
                    $gallery->page_id = $page->id;
                    $gallery->path = $img;
                    $gallery->save();
                }
            }

//            dd($request->item);
            if(isset($request->item)){
                $itemm = PageItem::where('page_id', $page->id)->get();
                foreach ($itemm as $iiikiii){
                    $iiikiii->delete();
                }
               foreach ($request->item as $value){
                   $item = new PageItem();
                   $item->page_id = $page->id;
                   $item->save();
                    foreach ($value as $index => $langus) {
                        $item->localizations()->create([
                            'domain_id' => $langus['domain_id'],
                            'page_id' => $page->id,
                            'page_item_id' => $item->id,
                            'lang' => $index,
                            'title' => $langus['title'],
                            'description' => $langus['description'],
                        ]);
                    }
                }
            }

            DB::commit();
            return redirect()->route('pages.index')->with('message', 'Successfully');
        }catch (\Exception $q){
            DB::rollBack();
            return [
                'message' => 'error',
                'e' => $q,
            ];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        $page->delete();
        return redirect()->route('pages.index')->with('message', 'Deleted');
    }
    public function deleteOne(Request $request)
    {
        $id = $request->id;
        $gallery = PageImage::findOrFail($id);
        $productImage = str_replace('/storage', '', $gallery->path);
        Storage::delete('/public' . $productImage);
        $gallery->delete();
        return response()->json($gallery,200);
    }


}
