<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Footer\CreateRequest;
use App\Http\Requests\Footer\EditRequest;
use App\Models\Footer;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FooterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $footers = Footer::all();
        return view('backend.dashboard.footer.index', compact('footers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.footer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        $filepicture = $request['image_one'];
        $firstfile = HellperFiles::FileOne($filepicture, null, 'Footer');
        $pathOne = HellperFiles::PathFile($firstfile);
        $footer = new Footer();
        $footer->path = $pathOne ;
        $footer->save();
        foreach ($request->langs as $key => $lang){
            $footer->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'description' => $lang['description'] ,
                'down_footer_one' => $lang['down_footer_one'] ,
                'down_footer_two' => $lang['down_footer_two'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('footer.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $footer = Footer::findorFail($id)->load('translateLang');
        return view('backend.dashboard.footer.edit', compact('footer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {
        $footer = Footer::findorFail($id);
        DB::beginTransaction();
        if ($request['image_one']){
            $filepicture = $request['image_one'];
            HellperFiles::FilesOneDelete($footer->path);
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Footer');
            $pathOne = HellperFiles::PathFile($firstfile);
            $footer->path = $pathOne ;
        }
        $deleteResult = $footer->localizations()->delete();
        if (!$deleteResult) {
            DB::rollBack();
            return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
        }
        $footer->save();
        //ADDING elements in ProductTr
        foreach ($request->langs as $key => $lang){
            $footer->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'description' => $lang['description'] ,
                'down_footer_one' => $lang['down_footer_one'] ,
                'down_footer_two' => $lang['down_footer_two'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('footer.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $footer = Footer::findorFail($id);
        HellperFiles::FilesOneDelete($footer->path);
        $footer->delete();
        $footer->localizations()->delete();
        return redirect()->route('footer.index')->with(['message' => 'Deleted.']);
    }



}
