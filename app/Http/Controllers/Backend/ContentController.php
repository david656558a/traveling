<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Content\CreateRequest;
use App\Http\Requests\Content\EditRequest;
use App\Models\Content;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $contents = Content::all();
        return view('backend.dashboard.content.index', compact('contents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.content.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
//        dd($request->all());

        DB::beginTransaction();
        $filepicture = $request['image_one'];
        $firstfile = HellperFiles::FileOne($filepicture, null, 'Content');
        $pathOne = HellperFiles::PathFile($firstfile);
        $content = new Content();
        $content->path = $pathOne ;
        $content->save();
        foreach ($request->langs as $key => $lang){
            $content->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('content.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $content = Content::findorFail($id)->load('translateLang');
        return view('backend.dashboard.content.edit', compact('content'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {

        $content = Content::findorFail($id);
        DB::beginTransaction();
        if ($request['image_one']){
            $filepicture = $request['image_one'];
            HellperFiles::FilesOneDelete($content->path);
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Content');
            $pathOne = HellperFiles::PathFile($firstfile);
            $content->path = $pathOne ;
        }
        $deleteResult = $content->localizations()->delete();
        if (!$deleteResult) {
            DB::rollBack();
            return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
        }
        $content->save();
        //ADDING elements in ProductTr
        foreach ($request->langs as $key => $lang){
            $content->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('content.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $content = Content::findorFail($id);
        HellperFiles::FilesOneDelete($content->path);
        $content->delete();
        $content->localizations()->delete();
        return redirect()->route('content.index')->with(['message' => 'Deleted.']);
    }

}
