<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Header\CreateRequest;
use App\Http\Requests\Header\EditRequest;
use App\Models\Header;
use App\Models\HeaderTranslate;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HeaderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $headers = Header::all();
        return view('backend.dashboard.header.index', compact('headers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.header.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateRequest $request)
    {
//        dd($request->all());

        DB::beginTransaction();
        $filepicture = $request['image_one'];
        $firstfile = HellperFiles::FileOne($filepicture, null, 'Header');
        $pathOne = HellperFiles::PathFile($firstfile);
        $header = new Header();
        $header->path = $pathOne ;
        $header->save();
        foreach ($request->langs as $key => $lang){
            $header->localizations()->create([
               'domain_id' => $lang['domain_id'] ,
               'lang' => $key ,
               'title' => $lang['title'] ,
               'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('header.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $header = Header::findorFail($id);
        return view('backend.dashboard.header.edit', compact('header'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(EditRequest $request, $id)
    {

        $header = Header::findorFail($id);
        DB::beginTransaction();
        if ($request['image_one']){
            $filepicture = $request['image_one'];
            HellperFiles::FilesOneDelete($header->path);
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Header');
            $pathOne = HellperFiles::PathFile($firstfile);
            $header->path = $pathOne ;
        }
        $deleteResult = $header->localizations()->delete();
        if (!$deleteResult) {
            DB::rollBack();
            return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
        }
        $header->save();
        //ADDING elements in ProductTr
        foreach ($request->langs as $key => $lang){
            $header->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('header.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $header = Header::findorFail($id);
        HellperFiles::FilesOneDelete($header->path);
        $header->delete();
        $header->localizations()->delete();
        return redirect()->route('header.index')->with(['message' => 'Deleted.']);
    }



}
