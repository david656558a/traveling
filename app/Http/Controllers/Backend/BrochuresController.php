<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Models\Brochure;
use App\Models\BrochureTranslate;
use App\MyHellper\HellperFiles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrochuresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $brochures = Brochure::all();
        return view('backend.dashboard.brochure.index', compact('brochures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('backend.dashboard.brochure.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
//        dd($request->all());
        DB::beginTransaction();
        $filepicture = $request['image_one'];
        $firstfile = HellperFiles::FileOne($filepicture, null, 'Brochure');
        $pathOne = HellperFiles::PathFile($firstfile);
        $brochure = new Brochure();
        $brochure->path = $pathOne ;
        $brochure->save();

        foreach ($request->langs as $key => $lang){
            $brochure->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'sub_title' => $lang['sub_title'] ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('brochure.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $brochure = Brochure::findorFail($id)->load('translateLang');
        return view('backend.dashboard.brochure.edit', compact('brochure'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $brochure = Brochure::findorFail($id);
        DB::beginTransaction();
        if ($request['image_one']){
            $filepicture = $request['image_one'];
            HellperFiles::FilesOneDelete($brochure->path);
            $firstfile = HellperFiles::FileOne($filepicture, null, 'Brochure');
            $pathOne = HellperFiles::PathFile($firstfile);
            $brochure->path = $pathOne ;
        }
        $deleteResult = $brochure->localizations()->delete();
        if (!$deleteResult) {
            DB::rollBack();
            return redirect()->back()->with(['error' => 'Something went wrong.'])->withInput();
        }
        $brochure->save();

        //ADDING elements in ProductTr
        foreach ($request->langs as $key => $lang){
            $brochure->localizations()->create([
                'domain_id' => $lang['domain_id'] ,
                'lang' => $key ,
                'sub_title' => $lang['sub_title'] ,
                'title' => $lang['title'] ,
                'description' => $lang['description'] ,
            ]);
        }

        DB::commit();

        return redirect()->route('brochure.index')->with(['message' => 'Successfully.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $brochure = Brochure::findorFail($id);
        HellperFiles::FilesOneDelete($brochure->path);
        $brochure->delete();
        $brochure->localizations()->delete();
        return redirect()->route('brochure.index')->with(['message' => 'Deleted.']);
    }
}
