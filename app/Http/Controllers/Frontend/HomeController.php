<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Brochure;
use App\Models\Content;
use App\Models\Destination;
use App\Models\Header;
use App\Models\Info;
use App\Models\News;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function home()
    {
        $news = News::first();
        $header = Header::first();
        $contents = Content::all();
        return view('frontend.pages.home', compact('news', 'header', 'contents'));
    }

    public function brochure()
    {
        $brochure = Brochure::first();
        return view('frontend.pages.brochure', compact('brochure'));
    }

    public function page($id)
    {
        $page = Page::findOrFail($id)->load('items', 'images');
        $extension = pathinfo(storage_path($page->path_header), PATHINFO_EXTENSION);
        return view('frontend.pages.pages', compact('page', 'extension'));
    }

    public function destination()
    {
        $destinations = Destination::all();
        return view('frontend.pages.destination', compact('destinations'));
    }

    public function destinationSingle($id)
    {
        $destination = Destination::findOrFail($id)->load('images');
        return view('frontend.pages.destination-single', compact('destination'));
    }

    public function hotelSingle()
    {
        return view('frontend.pages.hotel-single');
    }


}
