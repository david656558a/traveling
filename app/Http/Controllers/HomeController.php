<?php

namespace App\Http\Controllers;

use App\Models\Domain;
use App\Models\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $test = Test::where('domain_id', Cache::get('domain_id'))->first();
        dd($test);
        return view('zdefoultLaravel.home', compact('test'));
    }
}
