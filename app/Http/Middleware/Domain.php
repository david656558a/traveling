<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class Domain
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $currentDomain = $request->getHttpHost();
        $model = \App\Models\Domain::where('domain', $currentDomain)->first();
//        dd($model);
        if ($model === null){
            return $next($request);
        }
        $lang = $model->lang;
        session()->put('lang', $lang);
        session()->save();
        return $next($request);
    }
}
