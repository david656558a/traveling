<?php


namespace App\Http\View;

use App\Models\Domain;
use App\Models\Footer;
use App\Models\Info;
use App\Models\Page;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FooterComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = Footer::first();
        $view->with('footerGLOBAL', $data);
    }
}
