<?php


namespace App\Http\View;

use App\Models\Domain;
use App\Models\Page;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class MenuComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = Page::all();
        $view->with('menuNameGLOBAL', $data);
    }
}
