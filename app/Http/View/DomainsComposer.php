<?php


namespace App\Http\View;

use App\Models\Domain;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class DomainsComposer
{
    public function __construct()
    {
    }
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = Domain::all();
        $view->with('domainsGLOBAL', $data);
    }
}
