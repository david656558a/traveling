<?php

namespace App\Http\Requests\Pages;

use App\MyHellper\HellperDomainLangTrait;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    use HellperDomainLangTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'array',
            'langs.*' => 'array',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title' => 'required|max:255',
            'langs.'.$this->getFirstLang()['lang'].'.title' => 'required|max:255',
            'menu_name' => 'required|max:255',
        ];
    }

    public function messages()
    {
        return [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title.required' => 'Please write '.$this->getFirstLang()['name'].' Sub Title',
            'langs.'.$this->getFirstLang()['lang'].'.title.required' => 'Please write '.$this->getFirstLang()['name'].' Title',
            'menu_name.required' => 'Something went wrong: Menu Name',
        ];
    }
}
