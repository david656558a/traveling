<?php

namespace App\Http\Requests\Footer;

use App\MyHellper\HellperDomainLangTrait;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    use HellperDomainLangTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'array',
            'langs.*' => 'array',
            'langs.'.$this->getFirstLang()['lang'].'.description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
            'langs.'.$this->getFirstLang()['lang'].'.description.required' => 'Please write '.$this->getFirstLang()['name'].' Description',
        ];
    }
}
