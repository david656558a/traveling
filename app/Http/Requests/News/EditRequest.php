<?php

namespace App\Http\Requests\News;

use App\MyHellper\HellperDomainLangTrait;
use Illuminate\Foundation\Http\FormRequest;

class EditRequest extends FormRequest
{
    use HellperDomainLangTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lang' => 'array',
            'lang.*' => 'array',
            'lang.'.$this->getFirstLang()['lang'].'.title' => 'required|max:255',
            'lang.'.$this->getFirstLang()['lang'].'.description' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'lang.array' => 'Something went wrong',
            'lang.*.array' => 'Something went wrong',
            'lang.'.$this->getFirstLang()['lang'].'.title.required' => 'Please write '.$this->getFirstLang()['name'].' title',
            'lang.'.$this->getFirstLang()['lang'].'.description.required' => 'Please write '.$this->getFirstLang()['name'].' description',
        ];
    }
}
