<?php

namespace App\Http\Requests\Destination;

use App\MyHellper\HellperDomainLangTrait;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    use HellperDomainLangTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'langs' => 'array',
            'langs.*' => 'array',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title' => 'required|max:255',
            'langs.'.$this->getFirstLang()['lang'].'.title' => 'required|max:255',
            'langs.'.$this->getFirstLang()['lang'].'.description' => 'required',
            'image_one' => 'file|required',
            'image' => 'array|required',
        ];
    }

    public function messages()
    {
        return [
            'langs.array' => 'Something went wrong',
            'langs.*.array' => 'Something went wrong',
            'langs.'.$this->getFirstLang()['lang'].'.sub_title.required' => 'Please write '.$this->getFirstLang()['name'].' sub title',
            'langs.'.$this->getFirstLang()['lang'].'.title.required' => 'Please write '.$this->getFirstLang()['name'].' title',
            'langs.'.$this->getFirstLang()['lang'].'.description.required' => 'Please write '.$this->getFirstLang()['name'].' description',
        ];
    }
}
