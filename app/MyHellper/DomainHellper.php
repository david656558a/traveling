<?php


namespace App\MyHellper;


use App\Models\Domain;

class DomainHellper
{
    public static function attribute($model)
    {
        $lang = session()->get('lang');
        $translate = $model->localizations()->where('lang', $lang)->first();

        $langDef = Domain::first()->lang;
//        if (count($translate) == 0 && $langDef){
//
//        }
        $translateDefault = $model->localizations()->where('lang', $langDef)->first();
        if ($translate) {
            return $translate;
        }
        elseif($translateDefault){
            return $translateDefault;
        }
        return null;
    }
}
