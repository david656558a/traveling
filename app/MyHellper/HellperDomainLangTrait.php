<?php

namespace App\MyHellper;


use App\Models\Domain;

trait HellperDomainLangTrait
{
    public static function getFirstLang()
    {
        $domain = Domain::first();
        return  [
            'lang' => $domain->lang,
            'name' => $domain->name,
        ] ;
    }

}
