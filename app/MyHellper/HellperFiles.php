<?php

namespace App\MyHellper;


use Endroid\QrCode\ErrorCorrectionLevel;
use Endroid\QrCode\QrCode;
use Image;
use Illuminate\Support\Facades\Storage;


class HellperFiles
{
    ////////////////////////////////////////////////////// D. K. \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    /////////////////////////////////////////////// david656558a@gmail.com \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ////////////////////////////////////////////////// Version 0.0.1 \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    ///                                                                                                                  ///
    ///                                           php artisan storage:link                                               ///
    ///                                                                                                                  ///
    ///////////////////////////////////////////////////// FUNCTIONS ////////////////////////////////////////////////////////
    ////                                                                                                                ////
    ////        PathFile -            !!! FileOne                                                                       ////
    ////        NameFile -            !!! FileOne                                                                       ////
    ////                                                                                                                ////
    ////        FilesManyDelete -  FilesManyDelete(path)                                                                ////
    ////        FileOneDelete -    FileOneDelete(delFile ,path)                                                         ////
    ////                                                                                                                ////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////                                                                                                                ////
    ////        FilesMany -        FilesMany(files, path and 'path1', 'path2', 'path3', 'path4')                        ////
    ////        FileOne -          FileOne(file, path and 'path1', 'path2', 'path3', 'path4')                           ////
    ////                                                                                                                ////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////                                                                                                                ////
    //// ResizeImage -      ResizeImage(file, resizeWidth, $resizeHeight, path and 'path1', 'path2', 'path3', 'path4')  ////
    //// ResizeImages -     ResizeImages(file, resizeWidth, $resizeHeight, path and 'path1', 'path2', 'path3', 'path4') ////
    ////                                                                                                                ////
    ////                               composer require intervention/image                                              ////
    ////                                                                                                                ////
    ////                               config/app.php                                                                   ////
    ////                               $provides => [                                                                   ////
    ////                                   ......                                                                       ////
    ////                                   'Intervention\Image\ImageServiceProvider'                                    ////
    ////                               ],                                                                               ////
    ////                               $aliases => [                                                                    ////
    ////                                   .....                                                                        ////
    ////                                   'Image' => 'Intervention\Image\Facades\Image'                                ////
    ////                               ]                                                                                ////
    ////                                                                                                                ////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////                                                                                                                ////
    ////             VideoAndImage($file, $path2 = null, $path3 = null, $path4 = null, $path1 = 'nothing')              ////
    ////                                                                                                                ////
    ////                       <<   in one input you cаn create Video, Image, Gif different folders  >>                 ////
    ////                                    Folder[                                                                     ////
    ////                                             Video  -> storage in Video folter                                  ////
    ////                                             Images -> storage in Image folter                                  ////
    ////                                             Gif    -> storage in Gif folter                                    ////
    ////                                            ]                                                                   ////
    ////                                                                                                                ////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////                                                                                                                ////
    ////        FileQrCode -       FileQrCode(text)                                                                     ////
    ////                                                                                                                ////
    ////                           composer.json                                                                        ////
    ////                           "require": {                                                                         ////
    ////                                            "endroid/qr-code": "^3.9",                                          ////
    ////                                          },                                                                    ////
    ////                                                                                                                ////
    /////////////////////////////////////////////////////// example ////////////////////////////////////////////////////////
    ////                                                                                                                ////
    ////        FileOne -  [                                                                                            ////
    ////            $file = HellperFiles::FileOne(file, path and 'path1', 'path2', 'path3', 'path4');                   ////
    ////            $filePath = HellperFiles::PathFile($file);                                                          ////
    ////            $fileName = HellperFiles::NameFile($file);                                                          ////
    ////                                                       ]                                                        ////
    ////                                                                                                                ////
    ////        FilesMany - [                                                                                           ////
    ////            $file = HellperFiles::FilesMany(file, path and 'path1', 'path2', 'path3', 'path4');                 ////
    ////                                                       ]                                                        ////                                                                                                  ////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static function generatePath ($slug1, $slug2 = null, $slug3 = null, $slug4 = null ) {
        if ($slug1 != '' && $slug2 == null && $slug3 == null && $slug4 == null){
            return storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $slug1 . DIRECTORY_SEPARATOR);
        } else if ($slug1 != '' && $slug2 != '' && $slug3 == null && $slug4 == null){
            return storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $slug1 . DIRECTORY_SEPARATOR. $slug2 . DIRECTORY_SEPARATOR );
        }else if ($slug1 != '' && $slug2 != '' && $slug3 != '' && $slug4 == null){
            return storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $slug1 . DIRECTORY_SEPARATOR. $slug2 . DIRECTORY_SEPARATOR . $slug3 . DIRECTORY_SEPARATOR);
        }else if ( $slug1 != '' && $slug2 != '' && $slug3 != '' && $slug4 != ''){
            return storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $slug1 . DIRECTORY_SEPARATOR. $slug2 . DIRECTORY_SEPARATOR . $slug3 . DIRECTORY_SEPARATOR. $slug4 . DIRECTORY_SEPARATOR);
        }else{
            return null;
        }
    }
    public static function generatePathFile ($filePath1, $filePath2 = null, $filePath3 = null, $filePath4 = null ) {
        if ($filePath1 != '' && $filePath2 == null && $filePath3 == null && $filePath4 == null){
            return "/storage/". $filePath1 ."/";
        }else if ($filePath1 != '' && $filePath2 != '' && $filePath3 == null && $filePath4 == null){
            return "/storage/". $filePath1 ."/". $filePath2 ."/";
        }else if ($filePath1 != '' && $filePath2 != '' && $filePath3 != '' && $filePath4 == null){
            return "/storage/". $filePath1 ."/". $filePath2 ."/". $filePath3 ."/";
        }else if ( $filePath1 != '' && $filePath2 != '' && $filePath3 != '' && $filePath4 != ''){
            return "/storage/". $filePath1 ."/". $filePath2 ."/". $filePath3 ."/". $filePath4 ."/";
        }else{
            return null;
        }
    }

    public static function PathFile ($fileArray)
    {
        foreach ($fileArray as $path){
           $path;
        }
        return $path;
    }
    public static function NameFile ($fileArray)
    {
        foreach ($fileArray as $key => $name){
            $key;
        }
        return $key;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        //////////////////////////////////////////////  FUNCTIONS  ////////////////////////////////////////////////////

    public static function FilesMany($file, $path = null, $path1 = null, $path2 = null, $path3 = null, $path4 = null): ? array
    {
        if ($path == null){
            $path = Self::generatePath($path1, $path2, $path3, $path4);
        }
        if (!$file) {
            return null;
        }
        foreach ($file as $key => $fileMany) {
            $extension = $fileMany->getClientOriginalExtension();
            $name = pathinfo($fileMany->getClientOriginalName(), PATHINFO_FILENAME);
            $data_name = md5($name.microtime());
            $data = $data_name.'.'.$extension;
            $res = $fileMany->move($path, $data);
            if (!$res) {
                return null;
            }
            if (!$data) return response()->json(['message' => 'Something went wrong files'], 400);
            $filepath = Self::generatePathFile($path1, $path2, $path3, $path4);
            $key = $data;
            $allPath[$key] = $filepath . $key;
        }
        return $allPath;
    }

    public static function FileOne($file, $path = null, $path1 = null, $path2 = null, $path3 = null, $path4 = null): ? array
    {

        if ($path == null){
            $path = Self::generatePath($path1, $path2, $path3, $path4);
        }
        if (!$file) {
            return null;
        }
            $extension = $file->getClientOriginalExtension();
            $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $data_name = md5($name.microtime());
            $data = $data_name.'.'.$extension;
            $res = $file->move($path, $data);
            if (!$res) {
                return null;
            }
            if (!$data) return response()->json(['message' => 'Something went wrong files'], 400);

            $filepath = Self::generatePathFile($path1, $path2, $path3, $path4);
            $key = $data;
            $allPath[$key] = $filepath . $key;
        return $allPath;
    }

    public static function FilesOneDelete($path)
    {
        $PathForDelete = str_replace('/storage', '', $path);
        Storage::delete('/public' . $PathForDelete);
        return $PathForDelete;

    }

    public static function FilesManyDelete($delProd ,$path): ? array
    {
        foreach ($delProd as $name) {
            $PathForDelete = str_replace('/storage', '', $path);
            Storage::delete('/public' . $PathForDelete);
        }
        return $PathForDelete;
    }

    public static function FileQrCode($text, $path = null, $path1 = null, $path2 = null, $path3 = null, $path4 = null, $width = 150 , $height = 200 , $size = 300 , $margin = 10): ?string
    {
        if ($path == null){
            $path = Self::generatePath($path1, $path2, $path3, $path4);
        }
        $qrCode = new QrCode($text);
        $qrCode->setSize($size);
        $qrCode->setMargin($margin);
        $qrCode->setEncoding('UTF-8');
        $qrCode->setWriterByName('png');
        $qrCode->setErrorCorrectionLevel(ErrorCorrectionLevel::HIGH());
        $qrCode->setForegroundColor(['r' => 0, 'g' => 0, 'b' => 0, 'a' => 0]);
        $qrCode->setBackgroundColor(['r' => 255, 'g' => 255, 'b' => 255, 'a' => 0]);
        $qrCode->setLogoSize($width, $height);
        $qrCode->setValidateResult(false);
        $qrCode->setRoundBlockSize(true);
        $qrCode->setWriterOptions(['exclude_xml_declaration' => true]);
        $qr_name = md5(microtime());
        $name = $qr_name.'.png';
        $qrCode->writeFile($path.''.$name);
        $dbPath = Self::generatePathFile($path1, $path2, $path3, $path4);

        return $dbPath;
    }

    public static function ResizeImages($file, $resizeWidth, $resizeHeight, $path = null, $path1 = null, $path2 = null, $path3 = null, $path4 = null ): ? array
    {
        if ($path == null){
            $path = Self::generatePath($path1, $path2, $path3, $path4);
        }
        if (!$file) {
            return null;
        }
        foreach ($file as $key => $fileMany) {
                $extension = $fileMany->getClientOriginalExtension();
                $name = pathinfo($fileMany->getClientOriginalName(), PATHINFO_FILENAME);
                $data_name = md5($name . microtime());
                $data = $data_name . '.' . $extension;
                $reses = $fileMany->move($path, $data);
                $res = Image::make($reses->getRealPath())->resize($resizeWidth, $resizeHeight);
                $endFile = $res->save($path . $data);
            if (!$endFile) {
                return null;
            }
            $filepath = Self::generatePathFile($path1, $path2, $path3, $path4);
            $key = $data;
            $allPath[$key] = $filepath . $key;
        }
        return $allPath;
    }

    public static function ResizeImage($file, $resizeWidth, $resizeHeight, $path = null, $path1 = null, $path2 = null, $path3 = null, $path4 = null ): ? array
    {
        if ($path == null){
            $path = Self::generatePath($path1, $path2, $path3, $path4);
        }
        if (!$file) {
            return null;
        }
        if (@is_array(getimagesize($file))) {
            $extension = $file->getClientOriginalExtension();
            $name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $data_name = md5($name . microtime());
            $data = $data_name. '.' . $extension;

            $reses = $file->move($path, $data);
            $res = Image::make($reses->getRealPath())->resize($resizeWidth, $resizeHeight);
            $endFile = $res->save($path . $data);
        }
        if (!$endFile) {
            return null;
        }
        $filepath = Self::generatePathFile($path1, $path2, $path3, $path4);
        $key = $data;
        $allPath[$key] = $filepath . $key;

        return $allPath;
    }

    public static function VideoAndImage($file, $path2 = null, $path3 = null, $path4 = null, $path1 = 'nothing'): ? array
    {
        if (!$file) {
            return null;
        }
        foreach ($file as $key => $fileMany) {
            $extension = $fileMany->getClientOriginalExtension();
            if ($extension == 'mp4' || $extension == 'mov' || $extension == 'wmv' || $extension == 'flv' || $extension == 'avi' || $extension == '3gp'){
                    $path1 = 'Video';
                    $path = Self::generatePath($path1, $path2, $path3, $path4);
            } elseif ($extension == 'jpg' || $extension == 'png' || $extension == 'svg' || $extension == 'jpeg'){
                    $path1 = 'Image';
                    $path = Self::generatePath($path1, $path2, $path3, $path4);
            }elseif($extension == 'gif'){
                $path1 = 'Gif';
                $path = Self::generatePath($path1, $path2, $path3, $path4);
            }else{
                dd("MESSAGE: This format is not available");
            }
            $name = pathinfo($fileMany->getClientOriginalName(), PATHINFO_FILENAME);
            $data_name = md5($name . microtime());
            $data = $data_name . '.' . $extension;
            $res = $fileMany->move($path, $data);
            if (!$res) {
                return null;
            }
            if (!$data) return response()->json(['message' => 'Something went wrong files'], 400);
            $filepath = Self::generatePathFile($path1, $path2, $path3, $path4);
            $key = $data;
            $allPath[$key] = $filepath . $key;
        }
        return $allPath;
    }

}
