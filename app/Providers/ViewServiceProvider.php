<?php

namespace App\Providers;

use App\Http\View\DomainsComposer;
use App\Http\View\FooterComposer;
use App\Http\View\InfoComposer;
use App\Http\View\MenuComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('*', DomainsComposer::class);
        View::composer('*', MenuComposer::class);
        View::composer('*', InfoComposer::class);
        View::composer('*', FooterComposer::class);
    }
}
