<?php

namespace Database\Seeders;

use App\Models\Domain;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;


class DomainSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = Domain::create([
            'domain' => 'traveling4049ru.loc',
            'name' => 'Russian',
            'lang' => 'ru',
        ]);
        $admin = Domain::create([
            'domain' => 'traveling4049es.loc',
            'name' => 'English',
            'lang' => 'es',
        ]);

    }
}
